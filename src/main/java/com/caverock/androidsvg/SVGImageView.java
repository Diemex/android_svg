/*
   Copyright 2013 Paul LeBeau, Cave Rock Software Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.caverock.androidsvg;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.caverock.svgandroid.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;

/**
 * SVGImageView is a View widget that allows users to include SVG images in their layouts.
 * <p/>
 * It is implemented as a thin layer over {@code android.widget.ImageView}.
 * <p/>
 * In its present form it has one significant limitation.  It uses the {@link SVG#renderToPicture()}
 * method. That means that SVG documents that use {@code <mask>} elements will not display correctly.
 *
 * @attr ref R.styleable#SVGImageView_svg
 */
public class SVGImageView extends ImageView
{
    private static Method setLayerTypeMethod = null;
    private WeakReference<ISvgLoadTask> workerTaskReference;


    {
        try
        {
            setLayerTypeMethod = View.class.getMethod("setLayerType", Integer.TYPE, Paint.class);
        } catch (NoSuchMethodException e)
        { /* do nothing */ }
    }


    public SVGImageView(Context context)
    {
        super(context);
    }


    public SVGImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs, 0);
        init(attrs, 0);
    }


    public SVGImageView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }


    private void init(AttributeSet attrs, int defStyle)
    {
        if (isInEditMode())
            return;

        TypedArray a = getContext().getTheme()
                .obtainStyledAttributes(attrs, R.styleable.SVGImageView, defStyle, 0);
        try
        {
            int resourceId = a.getResourceId(R.styleable.SVGImageView_svg, -1);
            if (resourceId != -1)
            {
                setImageResource(resourceId);
                return;
            }

            String url = a.getString(R.styleable.SVGImageView_svg);
            if (url != null)
            {
                Uri uri = Uri.parse(url);
                if (internalSetImageURI(uri, false))
                    return;

                // Last chance, try loading it as an asset filename
                setImageAsset(url);
            }

        } finally
        {
            a.recycle();
        }
    }


    /**
     * Directly set the SVG.
     */
    public void setSVG(SVG mysvg)
    {
        if (mysvg == null)
            throw new IllegalArgumentException("Null value passed to setSVG()");

        setSoftwareLayerType();
        setImageDrawable(new PictureDrawable(mysvg.renderToPicture()));
    }


    /**
     * Load an SVG image from the given resource id.
     */
    @Override
    public void setImageResource(int resourceId)
    {
        if (cancelPotentialWork(resourceId))
        {
            LoadResourceTask task = new LoadResourceTask(this);
            workerTaskReference = new WeakReference<ISvgLoadTask>(task);
            task.execute(resourceId);
        }
    }


    /**
     * Load an SVG image from the given resource URI.
     */
    @Override
    public void setImageURI(Uri uri)
    {
        internalSetImageURI(uri, true);
    }


    /**
     * Load an SVG image from the given asset filename.
     */
    public void setImageAsset(String filename)
    {
        if (cancelPotentialWork(filename))
        {
            LoadAssetTask task = new LoadAssetTask(this);
            workerTaskReference = new WeakReference<ISvgLoadTask>(task);
            task.execute(filename);
        }
    }


    /**
     * Attempt to set a picture from a Uri. Return true if it worked.
     */
    private boolean internalSetImageURI(Uri uri, boolean isDirectRequestFromUser)
    {
        if (cancelPotentialWork(uri))
        {
            InputStream is;
            LoadURITask task;
            try
            {
                is = getContext().getContentResolver().openInputStream(uri);
                task = new LoadURITask(this, uri);
                workerTaskReference = new WeakReference<ISvgLoadTask>(task);
                task.execute(is);
            } catch (FileNotFoundException e)
            {
                if (isDirectRequestFromUser)
                    Log.e("SVGImageView", "File not found: " + uri);
                return false;
            }
        }
        return true;
    }


    //===============================================================================================


    private static class LoadResourceTask extends AsyncTask<Integer, Integer, Picture> implements ISvgLoadTask
    {
        private final WeakReference<SVGImageView> imageViewReference;
        private Integer data;


        protected LoadResourceTask(SVGImageView svgImageView)
        {
            imageViewReference = new WeakReference<SVGImageView>(svgImageView);
        }


        protected Picture doInBackground(Integer... resourceId)
        {
            data = resourceId[0];
            try
            {
                SVG svg = SVG.getFromResource(imageViewReference.get().getContext(), resourceId[0]);
                return svg.renderToPicture();
            } catch (SVGParseException e)
            {
                Log.e("SVGImageView", String.format("Error loading resource 0x%d: %s", data, e.getMessage()));
            }
            return null;
        }


        protected void onPostExecute(Picture picture)
        {
            SVGImageView imgView = imageViewReference.get();
            if (picture != null && !isCancelled() && imgView != null)
            {
                imgView.setSoftwareLayerType();
                imgView.setImageDrawable(new PictureDrawable(picture));
            }
        }


        @Override
        public boolean hasSameData(Object data)
        {
            return data instanceof Integer && data.equals(this.data);
        }


        @Override
        public void cancelWork()
        {
            cancel(true);
        }
    }


    private static class LoadAssetTask extends AsyncTask<String, Integer, Picture> implements ISvgLoadTask
    {
        private final WeakReference<SVGImageView> imageViewReference;
        private String data;


        protected LoadAssetTask(SVGImageView svgImageView)
        {
            imageViewReference = new WeakReference<SVGImageView>(svgImageView);
        }


        protected Picture doInBackground(String... filename)
        {
            data = filename[0];
            try
            {
                SVG svg = SVG.getFromAsset(imageViewReference.get().getContext().getAssets(), data);
                return svg.renderToPicture();
            } catch (SVGParseException e)
            {
                Log.e("SVGImageView", "Error loading file " + data + ": " + e.getMessage());
            } catch (FileNotFoundException e)
            {
                Log.e("SVGImageView", "File not found: " + data);
            } catch (IOException e)
            {
                Log.e("SVGImageView", "Unable to load asset file: " + data, e);
            }
            return null;
        }


        protected void onPostExecute(Picture picture)
        {
            SVGImageView imgView = imageViewReference.get();
            if (picture != null && imgView != null)
            {
                imgView.setSoftwareLayerType();
                imgView.setImageDrawable(new PictureDrawable(picture));
            }
        }


        @Override
        public boolean hasSameData(Object data)
        {
            return data instanceof String && data.equals(this.data);
        }


        @Override
        public void cancelWork()
        {
            cancel(true);
        }
    }


    private static class LoadURITask extends AsyncTask<InputStream, Integer, Picture> implements ISvgLoadTask
    {
        private final WeakReference<SVGImageView> imageViewReference;
        private Uri data;


        protected LoadURITask(SVGImageView svgImageView, Uri uri)
        {
            imageViewReference = new WeakReference<SVGImageView>(svgImageView);
            data = uri;
        }


        protected Picture doInBackground(InputStream... is)
        {
            try
            {
                SVG svg = SVG.getFromInputStream(is[0]);
                return svg.renderToPicture();
            } catch (SVGParseException e)
            {
                Log.e("SVGImageView", "Parse error loading URI: " + e.getMessage());
            } finally
            {
                try
                {
                    is[0].close();
                } catch (IOException e)
                { /* do nothing */ }
            }
            return null;
        }


        protected void onPostExecute(Picture picture)
        {
            SVGImageView imgView = imageViewReference.get();
            if (picture != null && imgView != null)
            {
                imgView.setSoftwareLayerType();
                imgView.setImageDrawable(new PictureDrawable(picture));
            }
        }


        @Override
        public boolean hasSameData(Object data)
        {
            return data instanceof Uri && data.equals(this.data);
        }


        @Override
        public void cancelWork()
        {
            cancel(true);
        }
    }


    private interface ISvgLoadTask
    {
        /**
         * Does this task have the same data
         */
        public boolean hasSameData(Object data);

        /**
         * like cancel(true)
         */
        public void cancelWork();
    }


    private ISvgLoadTask getWorkerTask()
    {
        if (workerTaskReference != null)
            return workerTaskReference.get();
        return null;
    }


    public boolean cancelPotentialWork(Object data)
    {
        final ISvgLoadTask workerTask = getWorkerTask();

        if (workerTask != null)
        {
            if (!workerTask.hasSameData(data)) //Is loading something else
                workerTask.cancelWork();
            else
                return false;
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }


    //===============================================================================================


    /*
     * Use reflection to call an API 11 method from this library (which is configured with a minSdkVersion of 8)
     */
    private void setSoftwareLayerType()
    {
        if (setLayerTypeMethod == null)
            return;

        try
        {
            int LAYER_TYPE_SOFTWARE = View.class.getField("LAYER_TYPE_SOFTWARE").getInt(new View(getContext()));
            setLayerTypeMethod.invoke(this, LAYER_TYPE_SOFTWARE, null);
        } catch (Exception e)
        {
            Log.w("SVGImageView", "Unexpected failure calling setLayerType", e);
        }
    }
}
