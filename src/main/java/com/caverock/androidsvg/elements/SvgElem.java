package com.caverock.androidsvg.elements;

import java.util.HashMap;

/**
 * Definitions of svg elements
 * <p/>
 * Supported:
 * <ul>
 * <li>svg</li>
 * <li>a</li>
 * <li>circle</li>
 * <li>clipPath</li>
 * <li>defs</li>
 * <li>desc</li>
 * <li>ellipse</li>
 * <li>g</li>
 * <li>image</li>
 * <li>line</li>
 * <li>linearGradient</li>
 * <li>marker</li>
 * <li>mask</li>
 * <li>path</li>
 * <li>pattern</li>
 * <li>polygon</li>
 * <li>polyline</li>
 * <li>radialGradient</li>
 * <li>rect</li>
 * <li>solidColor</li>
 * <li>stop</li>
 * <li>style</li>
 * <li>switch</li>
 * <li>symbol</li>
 * <li>text</li>
 * <li>textPath</li>
 * <li>title</li>
 * <li>tref</li>
 * <li>tspan</li>
 * <li>use</li>
 * <li>view</li>
 * </ul>
 * <p/>
 * Not supported:
 * <ul>
 * <li>animateColor</li>
 * <li>animateMotion</li>
 * <li>animateTransform</li>
 * <li>altGlyph</li>
 * <li>altGlyphDef</li>
 * <li>altGlyphItem</li>
 * <li>animate</li>
 * <li>color-profile</li>
 * <li>cursor</li>
 * <li>feBlend</li>
 * <li>feColorMatrix</li>
 * <li>feComponentTransfer</li>
 * <li>feComposite</li>
 * <li>feConvolveMatrix</li>
 * <li>feDiffuseLighting</li>
 * <li>feDisplacementMap</li>
 * <li>feDistantLight</li>
 * <li>feFlood</li>
 * <li>feFuncA</li>
 * <li>feFuncB</li>
 * <li>feFuncG</li>
 * <li>feFuncR</li>
 * <li>feGaussianBlur</li>
 * <li>feImage</li>
 * <li>feMerge</li>
 * <li>feMergeNode</li>
 * <li>feMorphology</li>
 * <li>feOffset</li>
 * <li>fePointLight</li>
 * <li>feSpecularLighting</li>
 * <li>feSpotLight</li>
 * <li>feTile</li>
 * <li>feTurbulence</li>
 * <li>filter</li>
 * <li>font</li>
 * <li>font-face</li>
 * <li>font-face-format</li>
 * <li>font-face-name</li>
 * <li>font-face-src</li>
 * <li>font-face-uri</li>
 * <li>foreignObject</li>
 * <li>glyph</li>
 * <li>glyphRef</li>
 * <li>hkern</li>
 * <li>mask</li>
 * <li>metadata</li>
 * <li>missing-glyph</li>
 * <li>mpath</li>
 * <li>script</li>
 * <li>set</li>
 * <li>style</li>
 * <li>vkern</li>
 * </ul>
 */
public enum SvgElem
{
    svg,
    a,
    circle,
    clipPath,
    defs,
    desc,
    ellipse,
    g,
    image,
    line,
    linearGradient,
    marker,
    mask,
    path,
    pattern,
    polygon,
    polyline,
    radialGradient,
    rect,
    solidColor,
    stop,
    style,
    SWITCH,
    symbol,
    text,
    textPath,
    title,
    tref,
    tspan,
    use,
    view,
    UNSUPPORTED;

    private static HashMap<String, SvgElem> cache = new HashMap<String, SvgElem>();


    public static SvgElem fromString(String str)
    {
        // First check cache to see if it is there
        SvgElem elem = cache.get(str);
        if (elem != null)
            return elem;
        // Manual check for "switch" which is in upper case because it's a Java reserved identifier
        if (str.equals("switch"))
        {
            cache.put(str, SWITCH);
            return SWITCH;
        }
        // Do the (slow) Enum.valueOf()
        try
        {
            elem = valueOf(str);
            if (elem != SWITCH)
            {  // Don't allow matches with "SWITCH"
                cache.put(str, elem);
                return elem;
            }
        } catch (IllegalArgumentException e)
        {
            // Do nothing
        }
        // Unknown element name
        cache.put(str, UNSUPPORTED);
        return UNSUPPORTED;
    }
}


// Element types that we don't support. Those that are containers have their
// contents ignored.
//private static final String  TAG_ANIMATECOLOR        = "animateColor";
//private static final String  TAG_ANIMATEMOTION       = "animateMotion";
//private static final String  TAG_ANIMATETRANSFORM    = "animateTransform";
//private static final String  TAG_ALTGLYPH            = "altGlyph";
//private static final String  TAG_ALTGLYPHDEF         = "altGlyphDef";
//private static final String  TAG_ALTGLYPHITEM        = "altGlyphItem";
//private static final String  TAG_ANIMATE             = "animate";
//private static final String  TAG_COLORPROFILE        = "color-profile";
//private static final String  TAG_CURSOR              = "cursor";
//private static final String  TAG_FEBLEND             = "feBlend";
//private static final String  TAG_FECOLORMATRIX       = "feColorMatrix";
//private static final String  TAG_FECOMPONENTTRANSFER = "feComponentTransfer";
//private static final String  TAG_FECOMPOSITE         = "feComposite";
//private static final String  TAG_FECONVOLVEMATRIX    = "feConvolveMatrix";
//private static final String  TAG_FEDIFFUSELIGHTING   = "feDiffuseLighting";
//private static final String  TAG_FEDISPLACEMENTMAP   = "feDisplacementMap";
//private static final String  TAG_FEDISTANTLIGHT      = "feDistantLight";
//private static final String  TAG_FEFLOOD             = "feFlood";
//private static final String  TAG_FEFUNCA             = "feFuncA";
//private static final String  TAG_FEFUNCB             = "feFuncB";
//private static final String  TAG_FEFUNCG             = "feFuncG";
//private static final String  TAG_FEFUNCR             = "feFuncR";
//private static final String  TAG_FEGAUSSIANBLUR      = "feGaussianBlur";
//private static final String  TAG_FEIMAGE             = "feImage";
//private static final String  TAG_FEMERGE             = "feMerge";
//private static final String  TAG_FEMERGENODE         = "feMergeNode";
//private static final String  TAG_FEMORPHOLOGY        = "feMorphology";
//private static final String  TAG_FEOFFSET            = "feOffset";
//private static final String  TAG_FEPOINTLIGHT        = "fePointLight";
//private static final String  TAG_FESPECULARLIGHTING  = "feSpecularLighting";
//private static final String  TAG_FESPOTLIGHT         = "feSpotLight";
//private static final String  TAG_FETILE              = "feTile";
//private static final String  TAG_FETURBULENCE        = "feTurbulence";
//private static final String  TAG_FILTER              = "filter";
//private static final String  TAG_FONT                = "font";
//private static final String  TAG_FONTFACE            = "font-face";
//private static final String  TAG_FONTFACEFORMAT      = "font-face-format";
//private static final String  TAG_FONTFACENAME        = "font-face-name";
//private static final String  TAG_FONTFACESRC         = "font-face-src";
//private static final String  TAG_FONTFACEURI         = "font-face-uri";
//private static final String  TAG_FOREIGNOBJECT       = "foreignObject";
//private static final String  TAG_GLYPH               = "glyph";
//private static final String  TAG_GLYPHREF            = "glyphRef";
//private static final String  TAG_HKERN               = "hkern";
//private static final String  TAG_MASK                = "mask";
//private static final String  TAG_METADATA            = "metadata";
//private static final String  TAG_MISSINGGLYPH        = "missing-glyph";
//private static final String  TAG_MPATH               = "mpath";
//private static final String  TAG_SCRIPT              = "script";
//private static final String  TAG_SET                 = "set";
//private static final String  TAG_STYLE               = "style";
//private static final String  TAG_VKERN               = "vkern";
