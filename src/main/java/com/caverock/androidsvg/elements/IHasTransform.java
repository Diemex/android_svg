package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

public interface IHasTransform
{
    public void setTransform(Matrix matrix);
}
