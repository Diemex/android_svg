package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

// A <defs> object contains objects that are not rendered directly, but are instead
// referenced from other parts of the file.
public class SvgDefs extends SvgGroup implements INotDirectlyRendered
{
    public SvgDefs(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }
}
