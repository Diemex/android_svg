package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.NumberParser;
import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgPath extends SvgGraphicsElement
{
    public SvgPathDefinition d;
    public Float pathLength;


    public SvgPath(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    public void parseAttributes(Attributes attributes) throws SAXException
    {
        //TODO Handle Exceptions correctly
        super.parseAttributes(attributes);
        parseAttributesPath(attributes);
    }


    private void parseAttributesPath(Attributes attributes) throws SAXException
    {
        //TODO Handle Exceptions correctly
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case d:
                    d = SvgPathDefinition.parsePath(val);
                    break;
                case pathLength:
                    pathLength = NumberParser.parseFloat(val);
                    if (pathLength < 0f)
                        throw new SAXException("Invalid <path> element. pathLength cannot be negative");
                    break;
                default:
                    break;
            }
        }
    }
}
