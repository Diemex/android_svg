package com.caverock.androidsvg.elements;

import org.xml.sax.SAXException;

import java.util.List;

public interface ISvgContainer
{
    public List<SvgObject> getChildren();

    public void addChild(SvgObject elem) throws SAXException;
}
