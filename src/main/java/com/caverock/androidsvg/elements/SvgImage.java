package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgImage extends SvgPreserveAspectRatioContainer implements IHasTransform
{
    public String href;
    public SvgLength x;
    public SvgLength y;
    public SvgLength width;
    public SvgLength height;
    public Matrix transform;


    public SvgImage(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void setTransform(Matrix transform)
    {
        this.transform = transform;
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        SvgGraphicsElement.parseAttributesTransform(this, attributes);
        SvgConditionalElement.parseAttributesConditional(this, attributes);
        parseAttributesImage(attributes);
    }


    private void parseAttributesImage(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case x:
                    x = SvgLength.parseLength(val);
                    break;
                case y:
                    y = SvgLength.parseLength(val);
                    break;
                case width:
                    width = SvgLength.parseLength(val);
                    if (width.isNegative())
                        throw new SAXException("Invalid <use> element. width cannot be negative");
                    break;
                case height:
                    height = SvgLength.parseLength(val);
                    if (height.isNegative())
                        throw new SAXException("Invalid <use> element. height cannot be negative");
                    break;
                case href:
                    if (!SVGParser.XLINK_NAMESPACE.equals(attributes.getURI(i)))
                        break;
                    href = val;
                    break;
                case preserveAspectRatio:
                    parsePreserveAspectRatio(this, val);
                    break;
                default:
                    break;
            }
        }
    }

}
