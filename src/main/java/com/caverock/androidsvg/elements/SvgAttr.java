package com.caverock.androidsvg.elements;

import java.util.HashMap;

/**
 * Supported SVG attributes
 */
public enum SvgAttr
{
    CLASS,    // Upper case because 'class' is a reserved word. Handled as a special case.
    clip,
    clip_path,
    clipPathUnits,
    clip_rule,
    color,
    cx, cy,
    direction,
    dx, dy,
    fx, fy,
    d,
    display,
    fill,
    fill_rule,
    fill_opacity,
    font,
    font_family,
    font_size,
    font_weight,
    font_style,
    // font_size_adjust, font_stretch, font_variant,
    gradientTransform,
    gradientUnits,
    height,
    href,
    id,
    marker,
    marker_start, marker_mid, marker_end,
    markerHeight, markerUnits, markerWidth,
    mask,
    maskContentUnits, maskUnits,
    media,
    offset,
    opacity,
    orient,
    overflow,
    pathLength,
    patternContentUnits, patternTransform, patternUnits,
    points,
    preserveAspectRatio,
    r,
    refX,
    refY,
    requiredFeatures, requiredExtensions, requiredFormats, requiredFonts,
    rx, ry,
    solid_color, solid_opacity,
    spreadMethod,
    startOffset,
    stop_color, stop_opacity,
    stroke,
    stroke_dasharray,
    stroke_dashoffset,
    stroke_linecap,
    stroke_linejoin,
    stroke_miterlimit,
    stroke_opacity,
    stroke_width,
    style,
    systemLanguage,
    text_anchor,
    text_decoration,
    transform,
    type,
    vector_effect,
    version,
    viewBox,
    width,
    x, y,
    x1, y1,
    x2, y2,
    viewport_fill, viewport_fill_opacity,
    visibility,
    UNSUPPORTED;

    private static HashMap<String, SvgAttr> cache = new HashMap<String, SvgAttr>();


    public static SvgAttr fromString(String str)
    {
        // First check cache to see if it is there
        SvgAttr attr = cache.get(str);
        if (attr != null)
            return attr;
        // Do the (slow) Enum.valueOf()
        if (str.equals("class"))
        {
            cache.put(str, CLASS);
            return CLASS;
        }
        // Check for underscore in attribute - it could potentially confuse us
        if (str.indexOf('_') != -1)
        {
            cache.put(str, UNSUPPORTED);
            return UNSUPPORTED;
        }
        try
        {
            attr = valueOf(str.replace('-', '_'));
            if (attr != CLASS)
            {
                cache.put(str, attr);
                return attr;
            }
        } catch (IllegalArgumentException e)
        {
            // Do nothing
        }
        // Unknown attribute name
        cache.put(str, UNSUPPORTED);
        return UNSUPPORTED;
    }

}
