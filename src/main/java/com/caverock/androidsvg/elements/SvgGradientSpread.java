package com.caverock.androidsvg.elements;

public enum SvgGradientSpread
{
    pad,
    reflect,
    repeat
}
