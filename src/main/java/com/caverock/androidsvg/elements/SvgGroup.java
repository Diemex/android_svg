package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

// An SVG element that can contain other elements.
public class SvgGroup extends SvgConditionalContainer implements IHasTransform
{
    public Matrix transform;


    public SvgGroup(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        SvgGraphicsElement.parseAttributesTransform(this, attributes);
        if (!(this instanceof SvgDefs)) //SvgDefs inherits from SvgGroup but doesn't support conditionals.
            SvgConditionalElement.parseAttributesConditional(this, attributes);
    }


    @Override
    public void setTransform(Matrix transform)
    {
        this.transform = transform;
    }
}
