package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

// Any object in the tree that corresponds to an SVG element
public class SvgElement extends SvgElementBase
{
    public SvgBox boundingBox = null;


    public SvgElement(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }
}
