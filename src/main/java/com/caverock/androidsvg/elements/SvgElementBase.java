package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.CSSParser;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.List;

// Any object in the tree that corresponds to an SVG element
public abstract class SvgElementBase extends SvgObject
{
    public String id = null;
    public Boolean spacePreserve = null;
    public SvgStyle baseStyle = null;   // style defined by explicit style attributes in the element (eg. fill="black")
    public SvgStyle style = null;       // style expressed in a 'style' attribute (eg. style="fill:black")
    public List<String> classNames = null;  // contents of the 'class' attribute


    public SvgElementBase(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    /*
    * Parse the style attributes for an element.
    */
    public static void parseAttributesStyle(SvgElementBase obj, Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            if (val.length() == 0)
            { // The spec doesn't say how to handle empty style attributes.
                continue;             // Our strategy is just to ignore them.
            }
            //boolean  inherit = val.equals("inherit");

            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case style:
                    parseStyle(obj, val);
                    break;

                case CLASS:
                    obj.classNames = CSSParser.parseClassAttribute(val);
                    break;

                default:
                    if (obj.baseStyle == null)
                        obj.baseStyle = new SvgStyle();
                    SvgStyle.processStyleProperty(obj.baseStyle, attributes.getLocalName(i), attributes.getValue(i).trim());
                    break;
            }
        }
    }


    /*
        * Parse the 'style' attribute.
        */
    private static void parseStyle(SvgElementBase obj, String style) throws SAXException
    {
        TextScanner scan = new TextScanner(style.replaceAll("/\\*.*?\\*/", ""));  // regex strips block comments

        while (true)
        {
            String propertyName = scan.nextToken(':');
            scan.skipWhitespace();
            if (!scan.consume(':'))
                break;  // Syntax error. Stop processing CSS rules.
            scan.skipWhitespace();
            String propertyValue = scan.nextToken(';');
            if (propertyValue == null)
                break;  // Syntax error
            scan.skipWhitespace();
            if (scan.empty() || scan.consume(';'))
            {
                if (obj.style == null)
                    obj.style = new SvgStyle();
                SvgStyle.processStyleProperty(obj.style, propertyName, propertyValue);
                scan.skipWhitespace();
            }
        }
    }


    public void parseAttributes(Attributes attributes) throws SAXException
    {
        parseAttributesCore(attributes);
    }


    private void parseAttributesCore(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String qname = attributes.getQName(i);
            if (qname.equals("id") || qname.equals("xml:id"))
            {
                id = attributes.getValue(i).trim();
                break;
            } else if (qname.equals("xml:space"))
            {
                String val = attributes.getValue(i).trim();
                if ("default".equals(val))
                {
                    spacePreserve = Boolean.FALSE;
                } else if ("preserve".equals(val))
                {
                    spacePreserve = Boolean.TRUE;
                } else
                {
                    throw new SAXException("Invalid value for \"xml:space\" attribute: " + val);
                }
                break;
            }
        }
    }
}
