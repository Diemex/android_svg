package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.PreserveAspectRatio;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

import java.util.HashMap;

public class SvgPreserveAspectRatioContainer extends SvgConditionalContainer
{
    public static HashMap<String, PreserveAspectRatio.Alignment> aspectRatioKeywords = null;
    public PreserveAspectRatio preserveAspectRatio = null;


    public SvgPreserveAspectRatioContainer(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    public static synchronized void initialiseAspectRatioKeywordsMap()
    {
        aspectRatioKeywords = new HashMap<String, PreserveAspectRatio.Alignment>(10);
        aspectRatioKeywords.put(SVGParser.NONE, PreserveAspectRatio.Alignment.None);
        aspectRatioKeywords.put("xMinYMin", PreserveAspectRatio.Alignment.XMinYMin);
        aspectRatioKeywords.put("xMidYMin", PreserveAspectRatio.Alignment.XMidYMin);
        aspectRatioKeywords.put("xMaxYMin", PreserveAspectRatio.Alignment.XMaxYMin);
        aspectRatioKeywords.put("xMinYMid", PreserveAspectRatio.Alignment.XMinYMid);
        aspectRatioKeywords.put("xMidYMid", PreserveAspectRatio.Alignment.XMidYMid);
        aspectRatioKeywords.put("xMaxYMid", PreserveAspectRatio.Alignment.XMaxYMid);
        aspectRatioKeywords.put("xMinYMax", PreserveAspectRatio.Alignment.XMinYMax);
        aspectRatioKeywords.put("xMidYMax", PreserveAspectRatio.Alignment.XMidYMax);
        aspectRatioKeywords.put("xMaxYMax", PreserveAspectRatio.Alignment.XMaxYMax);
    }


    //TODO this should be an instance method
    public static void parsePreserveAspectRatio(SvgPreserveAspectRatioContainer obj, String val) throws SAXException
    {
        if (aspectRatioKeywords == null)
            initialiseAspectRatioKeywordsMap();

        TextScanner scan = new TextScanner(val);
        scan.skipWhitespace();

        PreserveAspectRatio.Alignment align = null;
        PreserveAspectRatio.Scale scale = null;

        String word = scan.nextToken();
        if ("defer".equals(word))
        {    // Ignore defer keyword
            scan.skipWhitespace();
            word = scan.nextToken();
        }
        align = aspectRatioKeywords.get(word);
        scan.skipWhitespace();

        if (!scan.empty())
        {
            String meetOrSlice = scan.nextToken();
            if (meetOrSlice.equals("meet"))
            {
                scale = PreserveAspectRatio.Scale.Meet;
            } else if (meetOrSlice.equals("slice"))
            {
                scale = PreserveAspectRatio.Scale.Slice;
            } else
            {
                throw new SAXException("Invalid preserveAspectRatio definition: " + val);
            }
        }
        obj.preserveAspectRatio = new PreserveAspectRatio(align, scale);
    }
}
