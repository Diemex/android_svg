package com.caverock.androidsvg.elements;

public interface ISvgTextChild
{
    public void setTextRoot(ISvgTextRoot obj);

    public ISvgTextRoot getTextRoot();
}
