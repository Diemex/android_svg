package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

public class SvgPolyLine extends SvgGraphicsElement
{
    public float[] points;
    protected static String tag = "polyline";


    public SvgPolyLine(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesPolyLine(attributes);
    }


    /**
     * Parse the "points" attribute. Used by both <polyline> and <polygon>.
     */
    protected void parseAttributesPolyLine(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            if (SvgAttr.fromString(attributes.getLocalName(i)) == SvgAttr.points)
            {
                TextScanner scan = new TextScanner(attributes.getValue(i));
                List<Float> points = new ArrayList<Float>();
                scan.skipWhitespace();

                while (!scan.empty())
                {
                    Float x = scan.nextFloat();
                    if (x == null)
                        throw new SAXException("Invalid <" + tag + "> points attribute. Non-coordinate content found in list.");
                    scan.skipCommaWhitespace();
                    Float y = scan.nextFloat();
                    if (y == null)
                        throw new SAXException("Invalid <" + tag + "> points attribute. There should be an even number of coordinates.");
                    scan.skipCommaWhitespace();
                    points.add(x);
                    points.add(y);
                }
                this.points = new float[points.size()];
                int j = 0;
                for (Float f : points)
                {
                    this.points[j++] = f;
                }
            }
        }
    }
}
