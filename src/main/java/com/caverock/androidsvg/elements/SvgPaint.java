package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVGParser;

import org.xml.sax.SAXException;

// What fill or stroke is
public abstract class SvgPaint implements Cloneable
{
    public static SvgPaint parseColourSpecifer(String val) throws SAXException
    {
        if (val.equals(SVGParser.NONE))
        {
            return null;
        } else if (val.equals(SVGParser.CURRENTCOLOR))
        {
            return CurrentColor.getInstance();
        } else
        {
            return SvgColour.parseColour(val);
        }
    }


    /*
        * Parse a paint specifier such as in the fill and stroke attributes.
        */
    public static SvgPaint parsePaintSpecifier(String val, String attrName) throws SAXException
    {
        if (val.startsWith("url("))
        {
            int closeBracket = val.indexOf(")");
            if (closeBracket == -1)
                throw new SAXException("Bad " + attrName + " attribute. Unterminated url() reference");

            String href = val.substring(4, closeBracket).trim();
            SvgPaint fallback = null;

            val = val.substring(closeBracket + 1).trim();
            if (val.length() > 0)
                fallback = parseColourSpecifer(val);
            return new SvgPaintReference(href, fallback);

        }
        return parseColourSpecifer(val);
    }
}
