package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.NumberParser;
import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.List;

public class SvgStop extends SvgElementBase implements ISvgContainer
{
    public Float offset;


    public SvgStop(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    // Dummy container methods. Stop is officially a container, but we
    // are not interested in any of its possible child elements.
    @Override
    public List<SvgObject> getChildren()
    {
        return SVG.EMPTY_CHILD_LIST;
    }


    @Override
    public void addChild(SvgObject elem) throws SAXException
    { /* do nothing */ }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        parseAttributesStop(attributes);
    }


    private void parseAttributesStop(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case offset:
                    offset = parseGradiantOffset(val);
                    break;
                default:
                    break;
            }
        }
    }


    private Float parseGradiantOffset(String val) throws SAXException
    {
        if (val.length() == 0)
            throw new SAXException("Invalid offset value in <stop> (empty string)");
        int end = val.length();
        boolean isPercent = false;

        if (val.charAt(val.length() - 1) == '%')
        {
            end -= 1;
            isPercent = true;
        }
        try
        {
            float scalar = NumberParser.parseFloat(val, 0, end);
            if (isPercent)
                scalar /= 100f;
            return (scalar < 0) ? 0 : (scalar > 100) ? 100 : scalar;
        } catch (NumberFormatException e)
        {
            throw new SAXException("Invalid offset value in <stop>: " + val, e);
        }
    }
}
