package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.NumberParser;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGAndroidRenderer;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @hide
 */
public class SvgLength implements Cloneable
{
    float value = 0;
    public SvgUnit unit = SvgUnit.px;


    public SvgLength(float value, SvgUnit unit)
    {
        this.value = value;
        this.unit = unit;
    }


    public SvgLength(float value)
    {
        this.value = value;
        this.unit = SvgUnit.px;
    }


    // Parse stroke-dasharray
    public static SvgLength[] parseStrokeDashArray(String val) throws SAXException
    {
        TextScanner scan = new TextScanner(val);
        scan.skipWhitespace();

        if (scan.empty())
            return null;

        SvgLength dash = scan.nextLength();
        if (dash == null)
            return null;
        if (dash.isNegative())
            throw new SAXException("Invalid stroke-dasharray. Dash segemnts cannot be negative: " + val);

        float sum = dash.floatValue();

        List<SvgLength> dashes = new ArrayList<SvgLength>();
        dashes.add(dash);
        while (!scan.empty())
        {
            scan.skipCommaWhitespace();
            dash = scan.nextLength();
            if (dash == null)  // must have hit something unexpected
                throw new SAXException("Invalid stroke-dasharray. Non-Length content found: " + val);
            if (dash.isNegative())
                throw new SAXException("Invalid stroke-dasharray. Dash segemnts cannot be negative: " + val);
            dashes.add(dash);
            sum += dash.floatValue();
        }

        // Spec (section 11.4) says if the sum of dash lengths is zero, it should
        // be treated as "none" ie a solid stroke.
        if (sum == 0f)
            return null;

        return dashes.toArray(new SvgLength[dashes.size()]);
    }


    /*
        * Parse a list of Length/Coords
        */
    public static List<SvgLength> parseLengthList(String val) throws SAXException
    {
        if (val.length() == 0)
            throw new SAXException("Invalid length list (empty string)");

        List<SvgLength> coords = new ArrayList<SvgLength>(1);

        TextScanner scan = new TextScanner(val);
        scan.skipWhitespace();

        while (!scan.empty())
        {
            Float scalar = scan.nextFloat();
            if (scalar == null)
                throw new SAXException("Invalid length list value: " + scan.ahead());
            SvgUnit unit = scan.nextUnit();
            if (unit == null)
                unit = SvgUnit.px;
            coords.add(new SvgLength(scalar, unit));
            scan.skipCommaWhitespace();
        }
        return coords;
    }


    /*
        * Parse an SVG 'Length' value (usually a coordinate).
        * Spec says: length ::= number ("em" | "ex" | "px" | "in" | "cm" | "mm" | "pt" | "pc" | "%")?
        */
    public static SvgLength parseLength(String val) throws SAXException
    {
        if (val.length() == 0)
            throw new SAXException("Invalid length value (empty string)");
        int end = val.length();
        SvgUnit unit = SvgUnit.px;
        char lastChar = val.charAt(end - 1);

        if (lastChar == '%')
        {
            end -= 1;
            unit = SvgUnit.percent;
        } else if (end > 2 && Character.isLetter(lastChar) && Character.isLetter(val.charAt(end - 2)))
        {
            end -= 2;
            String unitStr = val.substring(end);
            try
            {
                unit = SvgUnit.valueOf(unitStr.toLowerCase(Locale.US));
            } catch (IllegalArgumentException e)
            {
                throw new SAXException("Invalid length unit specifier: " + val);
            }
        }
        try
        {
            float scalar = NumberParser.parseFloat(val, 0, end);
            return new SvgLength(scalar, unit);
        } catch (NumberFormatException e)
        {
            throw new SAXException("Invalid length value: " + val, e);
        }
    }


    public float floatValue()
    {
        return value;
    }


    // Convert length to user units for a horizontally-related context.
    public float floatValueX(SVGAndroidRenderer renderer)
    {
        switch (unit)
        {
            case px:
                return value;
            case em:
                return value * renderer.getCurrentFontSize();
            case ex:
                return value * renderer.getCurrentFontXHeight();
            case in:
                return value * renderer.getDPI();
            case cm:
                return value * renderer.getDPI() / 2.54f;
            case mm:
                return value * renderer.getDPI() / 25.4f;
            case pt: // 1 point = 1/72 in
                return value * renderer.getDPI() / 72f;
            case pc: // 1 pica = 1/6 in
                return value * renderer.getDPI() / 6f;
            case percent:
                SvgBox viewPortUser = renderer.getCurrentViewPortInUserUnits();
                if (viewPortUser == null)
                    return value;  // Undefined in this situation - so just return value to avoid an NPE
                return value * viewPortUser.width / 100f;
            default:
                return value;
        }
    }


    // Convert length to user units for a vertically-related context.
    public float floatValueY(SVGAndroidRenderer renderer)
    {
        if (unit == SvgUnit.percent)
        {
            SvgBox viewPortUser = renderer.getCurrentViewPortInUserUnits();
            if (viewPortUser == null)
                return value;  // Undefined in this situation - so just return value to avoid an NPE
            return value * viewPortUser.height / 100f;
        }
        return floatValueX(renderer);
    }


    // Convert length to user units for a context that is not orientation specific.
    // For example, stroke width.
    public float floatValue(SVGAndroidRenderer renderer)
    {
        if (unit == SvgUnit.percent)
        {
            SvgBox viewPortUser = renderer.getCurrentViewPortInUserUnits();
            if (viewPortUser == null)
                return value;  // Undefined in this situation - so just return value to avoid an NPE
            float w = viewPortUser.width;
            float h = viewPortUser.height;
            if (w == h)
                return value * w / 100f;
            float n = (float) (Math.sqrt(w * w + h * h) / SVG.SQRT2);  // see spec section 7.10
            return value * n / 100f;
        }
        return floatValueX(renderer);
    }


    // Convert length to user units for a context that is not orientation specific.
    // For percentage values, use the given 'max' parameter to represent the 100% value.
    public float floatValue(SVGAndroidRenderer renderer, float max)
    {
        if (unit == SvgUnit.percent)
        {
            return value * max / 100f;
        }
        return floatValueX(renderer);
    }


    // For situations (like calculating the initial viewport) when we can only rely on
    // physical real world units.
    public float floatValue(float dpi)
    {
        switch (unit)
        {
            case px:
                return value;
            case in:
                return value * dpi;
            case cm:
                return value * dpi / 2.54f;
            case mm:
                return value * dpi / 25.4f;
            case pt: // 1 point = 1/72 in
                return value * dpi / 72f;
            case pc: // 1 pica = 1/6 in
                return value * dpi / 6f;
            case em:
            case ex:
            case percent:
            default:
                return value;
        }
    }


    public boolean isZero()
    {
        return value == 0f;
    }


    public boolean isNegative()
    {
        return value < 0f;
    }


    @Override
    public String toString()
    {
        return String.valueOf(value) + unit;
    }
}
