package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgTag extends SvgViewBoxContainer
{
    public SvgLength x;
    public SvgLength y;
    public SvgLength width;
    public SvgLength height;
    public String version;


    public SvgTag(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        parseAttributesSVG(attributes);
    }


    private void parseAttributesSVG(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case x:
                    x = SvgLength.parseLength(val);
                    break;
                case y:
                    y = SvgLength.parseLength(val);
                    break;
                case width:
                    width = SvgLength.parseLength(val);
                    if (width.isNegative())
                        throw new SAXException("Invalid <svg> element. width cannot be negative");
                    break;
                case height:
                    height = SvgLength.parseLength(val);
                    if (height.isNegative())
                        throw new SAXException("Invalid <svg> element. height cannot be negative");
                    break;
                case version:
                    version = val;
                    break;
                default:
                    break;
            }
        }
    }
}
