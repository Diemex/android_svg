package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgLine extends SvgGraphicsElement
{
    public SvgLength x1;
    public SvgLength y1;
    public SvgLength x2;
    public SvgLength y2;


    public SvgLine(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesLine(attributes);
    }


    private void parseAttributesLine(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case x1:
                    x1 = SvgLength.parseLength(val);
                    break;
                case y1:
                    y1 = SvgLength.parseLength(val);
                    break;
                case x2:
                    x2 = SvgLength.parseLength(val);
                    break;
                case y2:
                    y2 = SvgLength.parseLength(val);
                    break;
                default:
                    break;
            }
        }
    }
}
