package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgViewBoxContainer extends SvgPreserveAspectRatioContainer
{
    public SvgBox viewBox;


    public SvgViewBoxContainer(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    //TODO should be an instance method
    public static void parseAttributesViewBox(SvgViewBoxContainer obj, Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case viewBox:
                    obj.viewBox = SvgBox.parseViewBox(val);
                    break;
                case preserveAspectRatio:
                    parsePreserveAspectRatio(obj, val);
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesViewBox(this, attributes);
        SvgConditionalElement.parseAttributesConditional(this, attributes);
    }
}
