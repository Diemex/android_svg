package com.caverock.androidsvg.elements;

import android.graphics.RectF;

import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

public class SvgBox implements Cloneable
{
    public float minX, minY, width, height;


    public SvgBox(float minX, float minY, float width, float height)
    {
        this.minX = minX;
        this.minY = minY;
        this.width = width;
        this.height = height;
    }


    public static SvgBox fromLimits(float minX, float minY, float maxX, float maxY)
    {
        return new SvgBox(minX, minY, maxX - minX, maxY - minY);
    }


    /*
        * Parse a viewBox attribute.
        */
    public static SvgBox parseViewBox(String val) throws SAXException
    {
        TextScanner scan = new TextScanner(val);
        scan.skipWhitespace();

        Float minX = scan.nextFloat();
        scan.skipCommaWhitespace();
        Float minY = scan.nextFloat();
        scan.skipCommaWhitespace();
        Float width = scan.nextFloat();
        scan.skipCommaWhitespace();
        Float height = scan.nextFloat();

        if (minX == null || minY == null || width == null || height == null)
            throw new SAXException("Invalid viewBox definition - should have four numbers");
        if (width < 0)
            throw new SAXException("Invalid viewBox. width cannot be negative");
        if (height < 0)
            throw new SAXException("Invalid viewBox. height cannot be negative");

        return new SvgBox(minX, minY, width, height);
    }


    public RectF toRectF()
    {
        return new RectF(minX, minY, maxX(), maxY());
    }


    public float maxX()
    {
        return minX + width;
    }


    public float maxY()
    {
        return minY + height;
    }


    public void union(SvgBox other)
    {
        if (other.minX < minX) minX = other.minX;
        if (other.minY < minY) minY = other.minY;
        if (other.maxX() > maxX()) width = other.maxX() - minX;
        if (other.maxY() > maxY()) height = other.maxY() - minY;
    }


    public String toString()
    {
        return "[" + minX + " " + minY + " " + width + " " + height + "]";
    }
}
