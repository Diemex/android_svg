package com.caverock.androidsvg.elements;

public enum SvgUnit
{
    px,
    em,
    ex,
    in,
    cm,
    mm,
    pt,
    pc,
    percent
}
