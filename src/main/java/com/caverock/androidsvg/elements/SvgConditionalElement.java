package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

// Any element that can appear inside a <switch> element.
public class SvgConditionalElement extends SvgElement implements ISvgConditional
{
    public Set<String> requiredFeatures = null;
    public String requiredExtensions = null;
    public Set<String> systemLanguage = null;
    public Set<String> requiredFormats = null;
    public Set<String> requiredFonts = null;


    public SvgConditionalElement(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    public static void parseAttributesConditional(ISvgConditional obj, Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case requiredFeatures:
                    obj.setRequiredFeatures(SvgConditionalContainer.parseRequiredFeatures(val));
                    break;
                case requiredExtensions:
                    obj.setRequiredExtensions(val);
                    break;
                case systemLanguage:
                    obj.setSystemLanguage(SvgConditionalContainer.parseSystemLanguage(val));
                    break;
                case requiredFormats:
                    obj.setRequiredFormats(SvgConditionalContainer.parseRequiredFormats(val));
                    break;
                case requiredFonts:
                    List<String> fonts = SvgStyle.parseFontFamily(val);
                    Set<String> fontSet = (fonts != null) ? new HashSet<String>(fonts) : new HashSet<String>(0);
                    obj.setRequiredFonts(fontSet);
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void setRequiredFeatures(Set<String> features)
    {
        this.requiredFeatures = features;
    }


    @Override
    public Set<String> getRequiredFeatures()
    {
        return this.requiredFeatures;
    }


    @Override
    public void setRequiredExtensions(String extensions)
    {
        this.requiredExtensions = extensions;
    }


    @Override
    public String getRequiredExtensions()
    {
        return this.requiredExtensions;
    }


    @Override
    public void setSystemLanguage(Set<String> languages)
    {
        this.systemLanguage = languages;
    }


    @Override
    public Set<String> getSystemLanguage()
    {
        return this.systemLanguage;
    }


    @Override
    public void setRequiredFormats(Set<String> mimeTypes)
    {
        this.requiredFormats = mimeTypes;
    }


    @Override
    public Set<String> getRequiredFormats()
    {
        return this.requiredFormats;
    }


    @Override
    public void setRequiredFonts(Set<String> fontNames)
    {
        this.requiredFonts = fontNames;
    }


    @Override
    public Set<String> getRequiredFonts()
    {
        return this.requiredFonts;
    }
}
