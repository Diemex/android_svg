package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgRect extends SvgGraphicsElement
{
    public SvgLength x;
    public SvgLength y;
    public SvgLength width;
    public SvgLength height;
    public SvgLength rx;
    public SvgLength ry;


    public SvgRect(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesRect(attributes);
    }


    private void parseAttributesRect(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case x:
                    x = SvgLength.parseLength(val);
                    break;
                case y:
                    y = SvgLength.parseLength(val);
                    break;
                case width:
                    width = SvgLength.parseLength(val);
                    if (width.isNegative())
                        throw new SAXException("Invalid <rect> element. width cannot be negative");
                    break;
                case height:
                    height = SvgLength.parseLength(val);
                    if (height.isNegative())
                        throw new SAXException("Invalid <rect> element. height cannot be negative");
                    break;
                case rx:
                    rx = SvgLength.parseLength(val);
                    if (rx.isNegative())
                        throw new SAXException("Invalid <rect> element. rx cannot be negative");
                    break;
                case ry:
                    ry = SvgLength.parseLength(val);
                    if (ry.isNegative())
                        throw new SAXException("Invalid <rect> element. ry cannot be negative");
                    break;
                default:
                    break;
            }
        }
    }
}
