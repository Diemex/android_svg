package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.NumberParser;
import com.caverock.androidsvg.SVGParser;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SvgStyle implements Cloneable
{
    //All available style properties
    public static final long SPECIFIED_FILL = (1 << 0);
    public static final long SPECIFIED_FILL_RULE = (1 << 1);
    public static final long SPECIFIED_FILL_OPACITY = (1 << 2);
    public static final long SPECIFIED_STROKE = (1 << 3);
    public static final long SPECIFIED_STROKE_OPACITY = (1 << 4);
    public static final long SPECIFIED_STROKE_WIDTH = (1 << 5);
    public static final long SPECIFIED_STROKE_LINECAP = (1 << 6);
    public static final long SPECIFIED_STROKE_LINEJOIN = (1 << 7);
    public static final long SPECIFIED_STROKE_MITERLIMIT = (1 << 8);
    public static final long SPECIFIED_STROKE_DASHARRAY = (1 << 9);
    public static final long SPECIFIED_STROKE_DASHOFFSET = (1 << 10);
    public static final long SPECIFIED_OPACITY = (1 << 11);
    public static final long SPECIFIED_COLOR = (1 << 12);
    public static final long SPECIFIED_FONT_FAMILY = (1 << 13);
    public static final long SPECIFIED_FONT_SIZE = (1 << 14);
    public static final long SPECIFIED_FONT_WEIGHT = (1 << 15);
    public static final long SPECIFIED_FONT_STYLE = (1 << 16);
    public static final long SPECIFIED_TEXT_DECORATION = (1 << 17);
    public static final long SPECIFIED_TEXT_ANCHOR = (1 << 18);
    public static final long SPECIFIED_OVERFLOW = (1 << 19);
    public static final long SPECIFIED_CLIP = (1 << 20);
    public static final long SPECIFIED_MARKER_START = (1 << 21);
    public static final long SPECIFIED_MARKER_MID = (1 << 22);
    public static final long SPECIFIED_MARKER_END = (1 << 23);
    public static final long SPECIFIED_DISPLAY = (1 << 24);
    public static final long SPECIFIED_VISIBILITY = (1 << 25);
    public static final long SPECIFIED_STOP_COLOR = (1 << 26);
    public static final long SPECIFIED_STOP_OPACITY = (1 << 27);
    public static final long SPECIFIED_CLIP_PATH = (1 << 28);
    public static final long SPECIFIED_CLIP_RULE = (1 << 29);
    public static final long SPECIFIED_MASK = (1 << 30);
    public static final long SPECIFIED_SOLID_COLOR = (1L << 31);
    public static final long SPECIFIED_SOLID_OPACITY = (1L << 32);
    public static final long SPECIFIED_VIEWPORT_FILL = (1L << 33);
    public static final long SPECIFIED_VIEWPORT_FILL_OPACITY = (1L << 34);
    public static final long SPECIFIED_VECTOR_EFFECT = (1L << 35);


    protected static final long SPECIFIED_NON_INHERITING = SPECIFIED_DISPLAY | SPECIFIED_OVERFLOW | SPECIFIED_CLIP
            | SPECIFIED_CLIP_PATH | SPECIFIED_OPACITY | SPECIFIED_STOP_COLOR
            | SPECIFIED_STOP_OPACITY | SPECIFIED_MASK | SPECIFIED_SOLID_COLOR
            | SPECIFIED_SOLID_OPACITY | SPECIFIED_VIEWPORT_FILL
            | SPECIFIED_VIEWPORT_FILL_OPACITY | SPECIFIED_VECTOR_EFFECT;
    public static final long SPECIFIED_DIRECTION = (1L << 36);
    public static final long SPECIFIED_ALL = 0xffffffff;
    private static HashMap<String, SvgLength> fontSizeKeywords = null;
    public static HashMap<String, Integer> fontWeightKeywords = null;
    // Which properties have been explicitly specified by this element
    public long specifiedFlags = 0;

    public SvgPaint fill;
    public FillRule fillRule;
    public Float fillOpacity;

    public SvgPaint stroke;
    public Float strokeOpacity;
    public SvgLength strokeWidth;
    public LineCaps strokeLineCap;
    public LineJoin strokeLineJoin;
    public Float strokeMiterLimit;
    public SvgLength[] strokeDashArray;
    public SvgLength strokeDashOffset;

    public Float opacity; // master opacity of both stroke and fill

    public SvgColour color;

    public List<String> fontFamily;
    public SvgLength fontSize;
    public Integer fontWeight;
    public FontStyle fontStyle;
    public TextDecoration textDecoration;
    public TextDirection direction;

    public TextAnchor textAnchor;

    public Boolean overflow;  // true if overflow visible
    public CSSClipRect clip;

    public String markerStart;
    public String markerMid;
    public String markerEnd;

    public Boolean display;    // true if we should display
    public Boolean visibility; // true if visible

    public SvgColour stopColor;
    public Float stopOpacity;

    public String clipPath;
    public FillRule clipRule;

    public String mask;

    public SvgPaint solidColor;
    public Float solidOpacity;

    public SvgPaint viewportFill;
    public Float viewportFillOpacity;

    public VectorEffect vectorEffect;


    public static final int FONT_WEIGHT_NORMAL = 400;
    public static final int FONT_WEIGHT_BOLD = 700;
    public static final int FONT_WEIGHT_LIGHTER = -1;
    public static final int FONT_WEIGHT_BOLDER = +1;


    public static void processStyleProperty(SvgStyle style, String localName, String val) throws SAXException
    {
        if (val.length() == 0)
        { // The spec doesn't say how to handle empty style attributes.
            return;               // Our strategy is just to ignore them.
        }
        if (val.equals("inherit"))
            return;

        switch (SvgAttr.fromString(localName))
        {
            case fill:
                style.fill = SvgPaint.parsePaintSpecifier(val, "fill");
                style.specifiedFlags |= SPECIFIED_FILL;
                break;

            case fill_rule:
                style.fillRule = parseFillRule(val);
                style.specifiedFlags |= SPECIFIED_FILL_RULE;
                break;

            case fill_opacity:
                style.fillOpacity = parseOpacity(val);
                style.specifiedFlags |= SPECIFIED_FILL_OPACITY;
                break;

            case stroke:
                style.stroke = SvgPaint.parsePaintSpecifier(val, "stroke");
                style.specifiedFlags |= SPECIFIED_STROKE;
                break;

            case stroke_opacity:
                style.strokeOpacity = parseOpacity(val);
                style.specifiedFlags |= SPECIFIED_STROKE_OPACITY;
                break;

            case stroke_width:
                style.strokeWidth = SvgLength.parseLength(val);
                style.specifiedFlags |= SPECIFIED_STROKE_WIDTH;
                break;

            case stroke_linecap:
                style.strokeLineCap = parseStrokeLineCap(val);
                style.specifiedFlags |= SPECIFIED_STROKE_LINECAP;
                break;

            case stroke_linejoin:
                style.strokeLineJoin = parseStrokeLineJoin(val);
                style.specifiedFlags |= SPECIFIED_STROKE_LINEJOIN;
                break;

            case stroke_miterlimit:
                style.strokeMiterLimit = NumberParser.parseFloat(val);
                style.specifiedFlags |= SPECIFIED_STROKE_MITERLIMIT;
                break;

            case stroke_dasharray:
                if (SVGParser.NONE.equals(val))
                    style.strokeDashArray = null;
                else
                    style.strokeDashArray = SvgLength.parseStrokeDashArray(val);
                style.specifiedFlags |= SPECIFIED_STROKE_DASHARRAY;
                break;

            case stroke_dashoffset:
                style.strokeDashOffset = SvgLength.parseLength(val);
                style.specifiedFlags |= SPECIFIED_STROKE_DASHOFFSET;
                break;

            case opacity:
                style.opacity = parseOpacity(val);
                style.specifiedFlags |= SPECIFIED_OPACITY;
                break;

            case color:
                style.color = SvgColour.parseColour(val);
                style.specifiedFlags |= SPECIFIED_COLOR;
                break;

            case font:
                parseFont(style, val);
                break;

            case font_family:
                style.fontFamily = parseFontFamily(val);
                style.specifiedFlags |= SPECIFIED_FONT_FAMILY;
                break;

            case font_size:
                style.fontSize = parseFontSize(val);
                style.specifiedFlags |= SPECIFIED_FONT_SIZE;
                break;

            case font_weight:
                style.fontWeight = parseFontWeight(val);
                style.specifiedFlags |= SPECIFIED_FONT_WEIGHT;
                break;

            case font_style:
                style.fontStyle = parseFontStyle(val);
                style.specifiedFlags |= SPECIFIED_FONT_STYLE;
                break;

            case text_decoration:
                style.textDecoration = parseTextDecoration(val);
                style.specifiedFlags |= SPECIFIED_TEXT_DECORATION;
                break;

            case direction:
                style.direction = parseTextDirection(val);
                style.specifiedFlags |= SPECIFIED_DIRECTION;
                break;

            case text_anchor:
                style.textAnchor = parseTextAnchor(val);
                style.specifiedFlags |= SPECIFIED_TEXT_ANCHOR;
                break;

            case overflow:
                style.overflow = parseOverflow(val);
                style.specifiedFlags |= SPECIFIED_OVERFLOW;
                break;

            case marker:
                style.markerStart = SvgConditionalContainer.parseFunctionalIRI(val, localName);
                style.markerMid = style.markerStart;
                style.markerEnd = style.markerStart;
                style.specifiedFlags |= (SPECIFIED_MARKER_START | SPECIFIED_MARKER_MID | SPECIFIED_MARKER_END);
                break;

            case marker_start:
                style.markerStart = SvgConditionalContainer.parseFunctionalIRI(val, localName);
                style.specifiedFlags |= SPECIFIED_MARKER_START;
                break;

            case marker_mid:
                style.markerMid = SvgConditionalContainer.parseFunctionalIRI(val, localName);
                style.specifiedFlags |= SPECIFIED_MARKER_MID;
                break;

            case marker_end:
                style.markerEnd = SvgConditionalContainer.parseFunctionalIRI(val, localName);
                style.specifiedFlags |= SPECIFIED_MARKER_END;
                break;

            case display:
                if (val.indexOf('|') >= 0 || (SVGParser.VALID_DISPLAY_VALUES.indexOf('|' + val + '|') == -1))
                    throw new SAXException("Invalid value for \"display\" attribute: " + val);
                style.display = !val.equals(SVGParser.NONE);
                style.specifiedFlags |= SPECIFIED_DISPLAY;
                break;

            case visibility:
                if (val.indexOf('|') >= 0 || (SVGParser.VALID_VISIBILITY_VALUES.indexOf('|' + val + '|') == -1))
                    throw new SAXException("Invalid value for \"visibility\" attribute: " + val);
                style.visibility = val.equals("visible");
                style.specifiedFlags |= SPECIFIED_VISIBILITY;
                break;

            case stop_color:
                if (val.equals(SVGParser.CURRENTCOLOR))
                {
                    style.stopColor = CurrentColor.getInstance();
                } else
                {
                    style.stopColor = SvgColour.parseColour(val);
                }
                style.specifiedFlags |= SPECIFIED_STOP_COLOR;
                break;

            case stop_opacity:
                style.stopOpacity = parseOpacity(val);
                style.specifiedFlags |= SPECIFIED_STOP_OPACITY;
                break;

            case clip:
                style.clip = CSSClipRect.parseClip(val);
                style.specifiedFlags |= SPECIFIED_CLIP;
                break;

            case clip_path:
                style.clipPath = SvgConditionalContainer.parseFunctionalIRI(val, localName);
                style.specifiedFlags |= SPECIFIED_CLIP_PATH;
                break;

            case clip_rule:
                style.clipRule = parseFillRule(val);
                style.specifiedFlags |= SPECIFIED_CLIP_RULE;
                break;

            case mask:
                style.mask = SvgConditionalContainer.parseFunctionalIRI(val, localName);
                style.specifiedFlags |= SPECIFIED_MASK;
                break;

            case solid_color:
                if (val.equals(SVGParser.CURRENTCOLOR))
                {
                    style.solidColor = CurrentColor.getInstance();
                } else
                {
                    style.solidColor = SvgColour.parseColour(val);
                }
                style.specifiedFlags |= SPECIFIED_SOLID_COLOR;
                break;

            case solid_opacity:
                style.solidOpacity = parseOpacity(val);
                style.specifiedFlags |= SPECIFIED_SOLID_OPACITY;
                break;

            case viewport_fill:
                if (val.equals(SVGParser.CURRENTCOLOR))
                {
                    style.viewportFill = CurrentColor.getInstance();
                } else
                {
                    style.viewportFill = SvgColour.parseColour(val);
                }
                style.specifiedFlags |= SPECIFIED_VIEWPORT_FILL;
                break;

            case viewport_fill_opacity:
                style.viewportFillOpacity = parseOpacity(val);
                style.specifiedFlags |= SPECIFIED_VIEWPORT_FILL_OPACITY;
                break;

            case vector_effect:
                style.vectorEffect = parseVectorEffect(val);
                style.specifiedFlags |= SPECIFIED_VECTOR_EFFECT;
                break;

            default:
                break;
        }
    }


    // Parse a font style keyword
    private static FontStyle parseFontStyle(String val) throws SAXException
    {
        FontStyle fs = fontStyleKeyword(val);
        if (fs != null)
            return fs;
        else
            throw new SAXException("Invalid font-style property: " + val);
    }


    // Parse a font style keyword
    public static FontStyle fontStyleKeyword(String val)
    {
        // Italic is probably the most common, so test that first :)
        if ("italic".equals(val))
            return FontStyle.Italic;
        else if ("normal".equals(val))
            return FontStyle.Normal;
        else if ("oblique".equals(val))
            return FontStyle.Oblique;
        else
            return null;
    }


    // Parse a text decoration keyword
    private static TextDecoration parseTextDecoration(String val) throws SAXException
    {
        if ("none".equals(val))
            return TextDecoration.None;
        if ("underline".equals(val))
            return TextDecoration.Underline;
        if ("overline".equals(val))
            return TextDecoration.Overline;
        if ("line-through".equals(val))
            return TextDecoration.LineThrough;
        if ("blink".equals(val))
            return TextDecoration.Blink;
        throw new SAXException("Invalid text-decoration property: " + val);
    }


    // Parse a text decoration keyword
    private static TextDirection parseTextDirection(String val) throws SAXException
    {
        if ("ltr".equals(val))
            return TextDirection.LTR;
        if ("rtl".equals(val))
            return TextDirection.RTL;
        throw new SAXException("Invalid direction property: " + val);
    }


    // Parse fill rule
    private static FillRule parseFillRule(String val) throws SAXException
    {
        if ("nonzero".equals(val))
            return FillRule.NonZero;
        if ("evenodd".equals(val))
            return FillRule.EvenOdd;
        throw new SAXException("Invalid fill-rule property: " + val);
    }


    // Parse stroke-linecap
    private static LineCaps parseStrokeLineCap(String val) throws SAXException
    {
        if ("butt".equals(val))
            return LineCaps.Butt;
        if ("round".equals(val))
            return LineCaps.Round;
        if ("square".equals(val))
            return LineCaps.Square;
        throw new SAXException("Invalid stroke-linecap property: " + val);
    }


    // Parse stroke-linejoin
    private static LineJoin parseStrokeLineJoin(String val) throws SAXException
    {
        if ("miter".equals(val))
            return LineJoin.Miter;
        if ("round".equals(val))
            return LineJoin.Round;
        if ("bevel".equals(val))
            return LineJoin.Bevel;
        throw new SAXException("Invalid stroke-linejoin property: " + val);
    }


    // Parse a text anchor keyword
    private static TextAnchor parseTextAnchor(String val) throws SAXException
    {
        if ("start".equals(val))
            return TextAnchor.Start;
        if ("middle".equals(val))
            return TextAnchor.Middle;
        if ("end".equals(val))
            return TextAnchor.End;
        throw new SAXException("Invalid text-anchor property: " + val);
    }


    // Parse a text anchor keyword
    private static Boolean parseOverflow(String val) throws SAXException
    {
        if ("visible".equals(val) || "auto".equals(val))
            return Boolean.TRUE;
        if ("hidden".equals(val) || "scroll".equals(val))
            return Boolean.FALSE;
        throw new SAXException("Invalid toverflow property: " + val);
    }


    // Parse a vector effect keyword
    private static VectorEffect parseVectorEffect(String val) throws SAXException
    {
        if ("none".equals(val))
            return VectorEffect.None;
        if ("non-scaling-stroke".equals(val))
            return VectorEffect.NonScalingStroke;
        throw new SAXException("Invalid vector-effect property: " + val);
    }


    // Parse a font attribute
    // [ [ <'font-style'> || <'font-variant'> || <'font-weight'> ]? <'font-size'> [ / <'line-height'> ]? <'font-family'> ] | caption | icon | menu | message-box | small-caption | status-bar | inherit
    private static void parseFont(SvgStyle style, String val) throws SAXException
    {
        List<String> fontFamily = null;
        SvgLength fontSize = null;
        Integer fontWeight = null;
        FontStyle fontStyle = null;
        String fontVariant = null;

        // Start by checking for the fixed size standard system font names (which we don't support)
        if ("|caption|icon|menu|message-box|small-caption|status-bar|".indexOf('|' + val + '|') != -1)
            return;

        // Fist part: style/variant/weight (opt - one or more)
        TextScanner scan = new TextScanner(val);
        String item = null;
        while (true)
        {
            item = scan.nextToken('/');
            scan.skipWhitespace();
            if (item == null)
                throw new SAXException("Invalid font style attribute: missing font size and family");
            if (fontWeight != null && fontStyle != null)
                break;
            if (item.equals("normal"))  // indeterminate which of these this refers to
                continue;
            if (fontWeight == null)
            {
                fontWeight = fontWeightKeywords.get(item);
                if (fontWeight != null)
                    continue;
            }
            if (fontStyle == null)
            {
                fontStyle = fontStyleKeyword(item);
                if (fontStyle != null)
                    continue;
            }
            // Must be a font-variant keyword?
            if (fontVariant == null && item.equals("small-caps"))
            {
                fontVariant = item;
                continue;
            }
            // Not any of these. Break and try next section
            break;
        }

        // Second part: font size (reqd) and line-height (opt)
        fontSize = parseFontSize(item);

        // Check for line-height (which we don't support)
        if (scan.consume('/'))
        {
            scan.skipWhitespace();
            item = scan.nextToken();
            if (item == null)
                throw new SAXException("Invalid font style attribute: missing line-height");
            SvgLength.parseLength(item);
            scan.skipWhitespace();
        }

        // Third part: font family
        fontFamily = parseFontFamily(scan.restOfText());

        style.fontFamily = fontFamily;
        style.fontSize = fontSize;
        style.fontWeight = (fontWeight == null) ? FONT_WEIGHT_NORMAL : fontWeight;
        style.fontStyle = (fontStyle == null) ? FontStyle.Normal : fontStyle;
        style.specifiedFlags |= (SPECIFIED_FONT_FAMILY | SPECIFIED_FONT_SIZE | SPECIFIED_FONT_WEIGHT | SPECIFIED_FONT_STYLE);
    }


    // Parse a font family list
    public static List<String> parseFontFamily(String val) throws SAXException
    {
        List<String> fonts = null;
        TextScanner scan = new TextScanner(val);
        while (true)
        {
            String item = scan.nextQuotedString();
            if (item == null)
                item = scan.nextToken(',');
            if (item == null)
                break;
            if (fonts == null)
                fonts = new ArrayList<String>();
            fonts.add(item);
            scan.skipCommaWhitespace();
            if (scan.empty())
                break;
        }
        return fonts;
    }


    // Parse a font size keyword or numerical value
    private static SvgLength parseFontSize(String val) throws SAXException
    {
        if (fontSizeKeywords == null)
            initialiseFontSizeKeywordsMap();
        SvgLength size = fontSizeKeywords.get(val);
        if (size == null)
        {
            size = SvgLength.parseLength(val);
        }
        return size;
    }


    private static synchronized void initialiseFontSizeKeywordsMap()
    {
        fontSizeKeywords = new HashMap<String, SvgLength>(9);
        fontSizeKeywords.put("xx-small", new SvgLength(0.694f, SvgUnit.pt));
        fontSizeKeywords.put("x-small", new SvgLength(0.833f, SvgUnit.pt));
        fontSizeKeywords.put("small", new SvgLength(10.0f, SvgUnit.pt));
        fontSizeKeywords.put("medium", new SvgLength(12.0f, SvgUnit.pt));
        fontSizeKeywords.put("large", new SvgLength(14.4f, SvgUnit.pt));
        fontSizeKeywords.put("x-large", new SvgLength(17.3f, SvgUnit.pt));
        fontSizeKeywords.put("xx-large", new SvgLength(20.7f, SvgUnit.pt));
        fontSizeKeywords.put("smaller", new SvgLength(83.33f, SvgUnit.percent));
        fontSizeKeywords.put("larger", new SvgLength(120f, SvgUnit.percent));
    }


    /*
        * Parse an opacity value (a float clamped to the range 0..1).
        */
    private static float parseOpacity(String val) throws SAXException
    {
        float o = NumberParser.parseFloat(val);
        return (o < 0f) ? 0f : (o > 1f) ? 1f : o;
    }


    // Parse a font weight keyword or numerical value
    private static Integer parseFontWeight(String val) throws SAXException
    {
        if (fontWeightKeywords == null)
            initialiseFontWeightKeywordsMap();
        Integer wt = fontWeightKeywords.get(val);
        if (wt == null)
        {
            throw new SAXException("Invalid font-weight property: " + val);
        }
        return wt;
    }


    private static synchronized void initialiseFontWeightKeywordsMap()
    {
        fontWeightKeywords = new HashMap<String, Integer>(13);
        fontWeightKeywords.put("normal", FONT_WEIGHT_NORMAL);
        fontWeightKeywords.put("bold", FONT_WEIGHT_BOLD);
        fontWeightKeywords.put("bolder", FONT_WEIGHT_BOLDER);
        fontWeightKeywords.put("lighter", FONT_WEIGHT_LIGHTER);
        fontWeightKeywords.put("100", 100);
        fontWeightKeywords.put("200", 200);
        fontWeightKeywords.put("300", 300);
        fontWeightKeywords.put("400", 400);
        fontWeightKeywords.put("500", 500);
        fontWeightKeywords.put("600", 600);
        fontWeightKeywords.put("700", 700);
        fontWeightKeywords.put("800", 800);
        fontWeightKeywords.put("900", 900);
    }


    public enum FillRule
    {
        NonZero,
        EvenOdd
    }

    public enum LineCaps
    {
        Butt,
        Round,
        Square
    }

    public enum LineJoin
    {
        Miter,
        Round,
        Bevel
    }

    public enum FontStyle
    {
        Normal,
        Italic,
        Oblique
    }

    public enum TextAnchor
    {
        Start,
        Middle,
        End
    }

    public enum TextDecoration
    {
        None,
        Underline,
        Overline,
        LineThrough,
        Blink
    }

    public enum TextDirection
    {
        LTR,
        RTL
    }

    public enum VectorEffect
    {
        None,
        NonScalingStroke
    }


    public static SvgStyle getDefaultStyle()
    {
        SvgStyle def = new SvgStyle();
        def.specifiedFlags = SPECIFIED_ALL;
        //def.inheritFlags = 0;
        def.fill = SvgColour.BLACK;
        def.fillRule = FillRule.NonZero;
        def.fillOpacity = 1f;
        def.stroke = null;         // none
        def.strokeOpacity = 1f;
        def.strokeWidth = new SvgLength(1f);
        def.strokeLineCap = LineCaps.Butt;
        def.strokeLineJoin = LineJoin.Miter;
        def.strokeMiterLimit = 4f;
        def.strokeDashArray = null;
        def.strokeDashOffset = new SvgLength(0f);
        def.opacity = 1f;
        def.color = SvgColour.BLACK; // currentColor defaults to black
        def.fontFamily = null;
        def.fontSize = new SvgLength(12, SvgUnit.pt);
        def.fontWeight = FONT_WEIGHT_NORMAL;
        def.fontStyle = FontStyle.Normal;
        def.textDecoration = TextDecoration.None;
        def.direction = TextDirection.LTR;
        def.textAnchor = TextAnchor.Start;
        def.overflow = true;  // Overflow shown/visible for root, but not for other elements (see section 14.3.3).
        def.clip = null;
        def.markerStart = null;
        def.markerMid = null;
        def.markerEnd = null;
        def.display = Boolean.TRUE;
        def.visibility = Boolean.TRUE;
        def.stopColor = SvgColour.BLACK;
        def.stopOpacity = 1f;
        def.clipPath = null;
        def.clipRule = FillRule.NonZero;
        def.mask = null;
        def.solidColor = null;
        def.solidOpacity = 1f;
        def.viewportFill = null;
        def.viewportFillOpacity = 1f;
        def.vectorEffect = VectorEffect.None;
        return def;
    }


    // Called on the state.style object to reset the properties that don't inherit
    // from the parent style.
    public void resetNonInheritingProperties()
    {
        resetNonInheritingProperties(false);
    }


    public void resetNonInheritingProperties(boolean isRootSVG)
    {
        this.display = Boolean.TRUE;
        this.overflow = isRootSVG ? Boolean.TRUE : Boolean.FALSE;
        this.clip = null;
        this.clipPath = null;
        this.opacity = 1f;
        this.stopColor = SvgColour.BLACK;
        this.stopOpacity = 1f;
        this.mask = null;
        this.solidColor = null;
        this.solidOpacity = 1f;
        this.viewportFill = null;
        this.viewportFillOpacity = 1f;
        this.vectorEffect = VectorEffect.None;
    }


    @Override
    public Object clone()
    {
        SvgStyle obj;
        try
        {
            obj = (SvgStyle) super.clone();
            if (strokeDashArray != null)
            {
                obj.strokeDashArray = (SvgLength[]) strokeDashArray.clone();
            }
            return obj;
        } catch (CloneNotSupportedException e)
        {
            throw new InternalError(e.toString());
        }
    }
}
