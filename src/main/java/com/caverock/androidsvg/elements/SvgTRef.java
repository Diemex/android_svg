package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgTRef extends SvgTextContainer implements ISvgTextChild
{
    public String href;

    private ISvgTextRoot textRoot;


    public SvgTRef(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void setTextRoot(ISvgTextRoot obj)
    {
        this.textRoot = obj;
    }


    @Override
    public ISvgTextRoot getTextRoot()
    {
        return this.textRoot;
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesTRef(attributes);
    }


    private void parseAttributesTRef(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case href:
                    if (!SVGParser.XLINK_NAMESPACE.equals(attributes.getURI(i)))
                        break;
                    href = val;
                    break;
                default:
                    break;
            }
        }
    }
}
