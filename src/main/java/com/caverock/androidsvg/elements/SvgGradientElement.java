package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

public class SvgGradientElement extends SvgElementBase implements ISvgContainer
{
    public List<SvgObject> children = new ArrayList<SvgObject>();

    public Boolean gradientUnitsAreUser;
    public Matrix gradientTransform;
    public SvgGradientSpread spreadMethod;
    public String href;


    public SvgGradientElement(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public List<SvgObject> getChildren()
    {
        return children;
    }


    @Override
    public void addChild(SvgObject elem) throws SAXException
    {
        if (elem instanceof SvgStop)
            children.add(elem);
        else
            throw new SAXException("Gradient elements cannot contain " + elem + " elements.");
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        parseAttributesGradient(attributes);
    }


    private void parseAttributesGradient(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case gradientUnits:
                    if ("objectBoundingBox".equals(val))
                    {
                        gradientUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        gradientUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute gradientUnits");
                    }
                    break;
                case gradientTransform:
                    gradientTransform = SvgGraphicsElement.parseTransformList(val);
                    break;
                case spreadMethod:
                    try
                    {
                        spreadMethod = SvgGradientSpread.valueOf(val);
                    } catch (IllegalArgumentException e)
                    {
                        throw new SAXException("Invalid spreadMethod attribute. \"" + val + "\" is not a valid value.");
                    }
                    break;
                case href:
                    if (!SVGParser.XLINK_NAMESPACE.equals(attributes.getURI(i)))
                        break;
                    href = val;
                    break;
                default:
                    break;
            }
        }
    }
}
