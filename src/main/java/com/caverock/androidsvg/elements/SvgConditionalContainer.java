package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class SvgConditionalContainer extends SvgElement implements ISvgContainer, ISvgConditional
{
    public List<SvgObject> children = new ArrayList<SvgObject>();

    public Set<String> requiredFeatures = null;
    public String requiredExtensions = null;
    public Set<String> systemLanguage = null;
    public Set<String> requiredFormats = null;
    public Set<String> requiredFonts = null;


    public SvgConditionalContainer(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    // Parse the attribute that declares the list of languages, one of which
    // must be supported if we are to render this element
    public static Set<String> parseSystemLanguage(String val) throws SAXException
    {
        TextScanner scan = new TextScanner(val);
        HashSet<String> result = new HashSet<String>();

        while (!scan.empty())
        {
            String language = scan.nextToken();
            int hyphenPos = language.indexOf('-');
            if (hyphenPos != -1)
            {
                language = language.substring(0, hyphenPos);
            }
            // Get canonical version of language code in case it has changed (see the JavaDoc for Locale.getLanguage())
            language = new Locale(language, "", "").getLanguage();
            result.add(language);
            scan.skipWhitespace();
        }
        return result;
    }


    // Parse the attribute that declares the list of SVG features that must be
    // supported if we are to render this element
    public static Set<String> parseRequiredFeatures(String val) throws SAXException
    {
        TextScanner scan = new TextScanner(val);
        HashSet<String> result = new HashSet<String>();

        while (!scan.empty())
        {
            String feature = scan.nextToken();
            if (feature.startsWith(SVGParser.FEATURE_STRING_PREFIX))
            {
                result.add(feature.substring(SVGParser.FEATURE_STRING_PREFIX.length()));
            } else
            {
                // Not a feature string we recognise or support. (In order to avoid accidentally
                // matches with our truncated feature strings, we'll replace it with a string
                // we know for sure won't match anything.
                result.add("UNSUPPORTED");
            }
            scan.skipWhitespace();
        }
        return result;
    }


    // Parse the attribute that declares the list of MIME types that must be
    // supported if we are to render this element
    public static Set<String> parseRequiredFormats(String val) throws SAXException
    {
        TextScanner scan = new TextScanner(val);
        HashSet<String> result = new HashSet<String>();

        while (!scan.empty())
        {
            String mimetype = scan.nextToken();
            result.add(mimetype);
            scan.skipWhitespace();
        }
        return result;
    }


    public static String parseFunctionalIRI(String val, String attrName) throws SAXException
    {
        if (val.equals(SVGParser.NONE))
            return null;
        if (!val.startsWith("url(") || !val.endsWith(")"))
            throw new SAXException("Bad " + attrName + " attribute. Expected \"none\" or \"url()\" format");

        return val.substring(4, val.length() - 1).trim();
        // Unlike CSS, the SVG spec seems to indicate that quotes are not allowed in "url()" references
    }


    @Override
    public List<SvgObject> getChildren()
    {
        return children;
    }


    @Override
    public void addChild(SvgObject elem) throws SAXException
    {
        children.add(elem);
    }


    @Override
    public void setRequiredFeatures(Set<String> features)
    {
        this.requiredFeatures = features;
    }


    @Override
    public Set<String> getRequiredFeatures()
    {
        return this.requiredFeatures;
    }


    @Override
    public void setRequiredExtensions(String extensions)
    {
        this.requiredExtensions = extensions;
    }


    @Override
    public String getRequiredExtensions()
    {
        return this.requiredExtensions;
    }


    @Override
    public void setSystemLanguage(Set<String> languages)
    {
        this.systemLanguage = languages;
    }


    @Override
    public Set<String> getSystemLanguage()
    {
        return null;
    }


    @Override
    public void setRequiredFormats(Set<String> mimeTypes)
    {
        this.requiredFormats = mimeTypes;
    }


    @Override
    public Set<String> getRequiredFormats()
    {
        return this.requiredFormats;
    }


    @Override
    public void setRequiredFonts(Set<String> fontNames)
    {
        this.requiredFonts = fontNames;
    }


    @Override
    public Set<String> getRequiredFonts()
    {
        return this.requiredFonts;
    }
}
