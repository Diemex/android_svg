package com.caverock.androidsvg.elements;

public interface SvgPathInterface
{
    public void moveTo(float x, float y);

    public void lineTo(float x, float y);

    public void cubicTo(float x1, float y1, float x2, float y2, float x3, float y3);

    public void quadTo(float x1, float y1, float x2, float y2);

    public void arcTo(float rx, float ry, float xAxisRotation, boolean largeArcFlag, boolean sweepFlag, float x, float y);

    public void close();
}
