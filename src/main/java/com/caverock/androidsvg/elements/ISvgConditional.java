package com.caverock.androidsvg.elements;

import java.util.Set;

// Any element that can appear inside a <switch> element.
public interface ISvgConditional
{
    public void setRequiredFeatures(Set<String> features);

    public Set<String> getRequiredFeatures();

    public void setRequiredExtensions(String extensions);

    public String getRequiredExtensions();

    public void setSystemLanguage(Set<String> languages);

    public Set<String> getSystemLanguage();

    public void setRequiredFormats(Set<String> mimeTypes);

    public Set<String> getRequiredFormats();

    public void setRequiredFonts(Set<String> fontNames);

    public Set<String> getRequiredFonts();
}
