package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgEllipse extends SvgGraphicsElement
{
    public SvgLength cx;
    public SvgLength cy;
    public SvgLength rx;
    public SvgLength ry;


    public SvgEllipse(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesEllipse(attributes);
    }


    private void parseAttributesEllipse(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case cx:
                    cx = SvgLength.parseLength(val);
                    break;
                case cy:
                    cy = SvgLength.parseLength(val);
                    break;
                case rx:
                    rx = SvgLength.parseLength(val);
                    if (rx.isNegative())
                        throw new SAXException("Invalid <ellipse> element. rx cannot be negative");
                    break;
                case ry:
                    ry = SvgLength.parseLength(val);
                    if (ry.isNegative())
                        throw new SAXException("Invalid <ellipse> element. ry cannot be negative");
                    break;
                default:
                    break;
            }
        }
    }
}
