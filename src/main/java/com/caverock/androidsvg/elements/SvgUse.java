package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgUse extends SvgGroup
{
    public String href;
    public SvgLength x;
    public SvgLength y;
    public SvgLength width;
    public SvgLength height;


    public SvgUse(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesUse(attributes);
    }


    private void parseAttributesUse(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case x:
                    x = SvgLength.parseLength(val);
                    break;
                case y:
                    y = SvgLength.parseLength(val);
                    break;
                case width:
                    width = SvgLength.parseLength(val);
                    if (width.isNegative())
                        throw new SAXException("Invalid <use> element. width cannot be negative");
                    break;
                case height:
                    height = SvgLength.parseLength(val);
                    if (height.isNegative())
                        throw new SAXException("Invalid <use> element. height cannot be negative");
                    break;
                case href:
                    if (!SVGParser.XLINK_NAMESPACE.equals(attributes.getURI(i)))
                        break;
                    href = val;
                    break;
                default:
                    break;
            }
        }
    }
}
