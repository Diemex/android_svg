package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

// An SVG element that can contain other elements.
public class SvgSwitch extends SvgGroup
{
    public SvgSwitch(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }
}
