package com.caverock.androidsvg.elements;

import android.util.Log;

import com.caverock.androidsvg.SVGParser;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SvgPathDefinition implements SvgPathInterface
{
    private List<Byte> commands = null;
    private List<Float> coords = null;

    private static final byte MOVETO = 0;
    private static final byte LINETO = 1;
    private static final byte CUBICTO = 2;
    private static final byte QUADTO = 3;
    private static final byte ARCTO = 4;   // 4-7
    private static final byte CLOSE = 8;


    public SvgPathDefinition()
    {
        this.commands = new ArrayList<Byte>();
        this.coords = new ArrayList<Float>();
    }


    public boolean isEmpty()
    {
        return commands.isEmpty();
    }


    @Override
    public void moveTo(float x, float y)
    {
        commands.add(MOVETO);
        coords.add(x);
        coords.add(y);
    }


    @Override
    public void lineTo(float x, float y)
    {
        commands.add(LINETO);
        coords.add(x);
        coords.add(y);
    }


    @Override
    public void cubicTo(float x1, float y1, float x2, float y2, float x3, float y3)
    {
        commands.add(CUBICTO);
        coords.add(x1);
        coords.add(y1);
        coords.add(x2);
        coords.add(y2);
        coords.add(x3);
        coords.add(y3);
    }


    @Override
    public void quadTo(float x1, float y1, float x2, float y2)
    {
        commands.add(QUADTO);
        coords.add(x1);
        coords.add(y1);
        coords.add(x2);
        coords.add(y2);
    }


    @Override
    public void arcTo(float rx, float ry, float xAxisRotation, boolean largeArcFlag, boolean sweepFlag, float x, float y)
    {
        int arc = ARCTO | (largeArcFlag ? 2 : 0) | (sweepFlag ? 1 : 0);
        commands.add((byte) arc);
        coords.add(rx);
        coords.add(ry);
        coords.add(xAxisRotation);
        coords.add(x);
        coords.add(y);
    }


    @Override
    public void close()
    {
        commands.add(CLOSE);
    }


    public void enumeratePath(SvgPathInterface handler)
    {
        Iterator<Float> coordsIter = coords.iterator();

        for (byte command : commands)
        {
            switch (command)
            {
                case MOVETO:
                    handler.moveTo(coordsIter.next(), coordsIter.next());
                    break;
                case LINETO:
                    handler.lineTo(coordsIter.next(), coordsIter.next());
                    break;
                case CUBICTO:
                    handler.cubicTo(coordsIter.next(), coordsIter.next(), coordsIter.next(), coordsIter.next(), coordsIter.next(), coordsIter.next());
                    break;
                case QUADTO:
                    handler.quadTo(coordsIter.next(), coordsIter.next(), coordsIter.next(), coordsIter.next());
                    break;
                case CLOSE:
                    handler.close();
                    break;
                default:
                    boolean largeArcFlag = (command & 2) != 0;
                    boolean sweepFlag = (command & 1) != 0;
                    handler.arcTo(coordsIter.next(), coordsIter.next(), coordsIter.next(), largeArcFlag, sweepFlag, coordsIter.next(), coordsIter.next());
            }
        }
    }


    /**
     * Parse the string that defines a path.
     */
    public static SvgPathDefinition parsePath(String val) throws SAXException
    {
        TextScanner scan = new TextScanner(val);

        int pathCommand = '?';
        float currentX = 0f, currentY = 0f;    // The last point visited in the subpath
        float lastMoveX = 0f, lastMoveY = 0f;  // The initial point of current subpath
        float lastControlX = 0f, lastControlY = 0f;  // Last control point of the just completed bezier curve.
        Float x, y, x1, y1, x2, y2;
        Float rx, ry, xAxisRotation;
        Boolean largeArcFlag, sweepFlag;

        SvgPathDefinition path = new SvgPathDefinition();

        if (scan.empty())
            return path;

        pathCommand = scan.nextChar();

        if (pathCommand != 'M' && pathCommand != 'm')
            return path;  // Invalid path - doesn't start with a move

        while (true)
        {
            scan.skipWhitespace();

            switch (pathCommand)
            {
                // Move
                case 'M':
                case 'm':
                    x = scan.nextFloat();
                    y = scan.checkedNextFloat(x);
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    // Relative moveto at the start of a path is treated as an absolute moveto.
                    if (pathCommand == 'm' && !path.isEmpty())
                    {
                        x += currentX;
                        y += currentY;
                    }
                    path.moveTo(x, y);
                    currentX = lastMoveX = lastControlX = x;
                    currentY = lastMoveY = lastControlY = y;
                    // Any subsequent coord pairs should be treated as a lineto.
                    pathCommand = (pathCommand == 'm') ? 'l' : 'L';
                    break;

                // Line
                case 'L':
                case 'l':
                    x = scan.nextFloat();
                    y = scan.checkedNextFloat(x);
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 'l')
                    {
                        x += currentX;
                        y += currentY;
                    }
                    path.lineTo(x, y);
                    currentX = lastControlX = x;
                    currentY = lastControlY = y;
                    break;

                // Cubic bezier
                case 'C':
                case 'c':
                    x1 = scan.nextFloat();
                    y1 = scan.checkedNextFloat(x1);
                    x2 = scan.checkedNextFloat(y1);
                    y2 = scan.checkedNextFloat(x2);
                    x = scan.checkedNextFloat(y2);
                    y = scan.checkedNextFloat(x);
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 'c')
                    {
                        x += currentX;
                        y += currentY;
                        x1 += currentX;
                        y1 += currentY;
                        x2 += currentX;
                        y2 += currentY;
                    }
                    path.cubicTo(x1, y1, x2, y2, x, y);
                    lastControlX = x2;
                    lastControlY = y2;
                    currentX = x;
                    currentY = y;
                    break;

                // Smooth curve (first control point calculated)
                case 'S':
                case 's':
                    x1 = 2 * currentX - lastControlX;
                    y1 = 2 * currentY - lastControlY;
                    x2 = scan.nextFloat();
                    y2 = scan.checkedNextFloat(x2);
                    x = scan.checkedNextFloat(y2);
                    y = scan.checkedNextFloat(x);
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 's')
                    {
                        x += currentX;
                        y += currentY;
                        x2 += currentX;
                        y2 += currentY;
                    }
                    path.cubicTo(x1, y1, x2, y2, x, y);
                    lastControlX = x2;
                    lastControlY = y2;
                    currentX = x;
                    currentY = y;
                    break;

                // Close path
                case 'Z':
                case 'z':
                    path.close();
                    currentX = lastControlX = lastMoveX;
                    currentY = lastControlY = lastMoveY;
                    break;

                // Horizontal line
                case 'H':
                case 'h':
                    x = scan.nextFloat();
                    if (x == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 'h')
                    {
                        x += currentX;
                    }
                    path.lineTo(x, currentY);
                    currentX = lastControlX = x;
                    break;

                // Vertical line
                case 'V':
                case 'v':
                    y = scan.nextFloat();
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 'v')
                    {
                        y += currentY;
                    }
                    path.lineTo(currentX, y);
                    currentY = lastControlY = y;
                    break;

                // Quadratic bezier
                case 'Q':
                case 'q':
                    x1 = scan.nextFloat();
                    y1 = scan.checkedNextFloat(x1);
                    x = scan.checkedNextFloat(y1);
                    y = scan.checkedNextFloat(x);
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 'q')
                    {
                        x += currentX;
                        y += currentY;
                        x1 += currentX;
                        y1 += currentY;
                    }
                    path.quadTo(x1, y1, x, y);
                    lastControlX = x1;
                    lastControlY = y1;
                    currentX = x;
                    currentY = y;
                    break;

                // Smooth quadratic bezier
                case 'T':
                case 't':
                    x1 = 2 * currentX - lastControlX;
                    y1 = 2 * currentY - lastControlY;
                    x = scan.nextFloat();
                    y = scan.checkedNextFloat(x);
                    if (y == null)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 't')
                    {
                        x += currentX;
                        y += currentY;
                    }
                    path.quadTo(x1, y1, x, y);
                    lastControlX = x1;
                    lastControlY = y1;
                    currentX = x;
                    currentY = y;
                    break;

                // Arc
                case 'A':
                case 'a':
                    rx = scan.nextFloat();
                    ry = scan.checkedNextFloat(rx);
                    xAxisRotation = scan.checkedNextFloat(ry);
                    largeArcFlag = scan.checkedNextFlag(xAxisRotation);
                    sweepFlag = scan.checkedNextFlag(largeArcFlag);
                    x = scan.checkedNextFloat(sweepFlag);
                    y = scan.checkedNextFloat(x);
                    if (y == null || rx < 0 || ry < 0)
                    {
                        Log.e(SVGParser.TAG, "Bad path coords for " + ((char) pathCommand) + " path segment");
                        return path;
                    }
                    if (pathCommand == 'a')
                    {
                        x += currentX;
                        y += currentY;
                    }
                    path.arcTo(rx, ry, xAxisRotation, largeArcFlag, sweepFlag, x, y);
                    currentX = lastControlX = x;
                    currentY = lastControlY = y;
                    break;

                default:
                    return path;
            }

            scan.skipCommaWhitespace();
            if (scan.empty())
                break;

            // Test to see if there is another set of coords for the current path command
            if (scan.hasLetter())
            {
                // Nope, so get the new path command instead
                pathCommand = scan.nextChar();
            }
        }
        return path;
    }
}
