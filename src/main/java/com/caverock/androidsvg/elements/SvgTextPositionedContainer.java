package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.List;

public class SvgTextPositionedContainer extends SvgTextContainer
{
    public List<SvgLength> x;
    public List<SvgLength> y;
    public List<SvgLength> dx;
    public List<SvgLength> dy;


    public SvgTextPositionedContainer(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesTextPosition(attributes);
    }


    private void parseAttributesTextPosition(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case x:
                    x = SvgLength.parseLengthList(val);
                    break;
                case y:
                    y = SvgLength.parseLengthList(val);
                    break;
                case dx:
                    dx = SvgLength.parseLengthList(val);
                    break;
                case dy:
                    dy = SvgLength.parseLengthList(val);
                    break;
                default:
                    break;
            }
        }
    }
}
