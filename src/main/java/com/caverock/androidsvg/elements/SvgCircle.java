package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgCircle extends SvgGraphicsElement
{
    public SvgLength cx;
    public SvgLength cy;
    public SvgLength r;


    public SvgCircle(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesCircle(attributes);
    }


    private void parseAttributesCircle(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case cx:
                    cx = SvgLength.parseLength(val);
                    break;
                case cy:
                    cy = SvgLength.parseLength(val);
                    break;
                case r:
                    r = SvgLength.parseLength(val);
                    if (r.isNegative())
                        throw new SAXException("Invalid <circle> element. r cannot be negative");
                    break;
                default:
                    break;
            }
        }
    }
}
