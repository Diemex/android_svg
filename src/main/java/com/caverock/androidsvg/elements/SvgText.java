package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgText extends SvgTextPositionedContainer implements ISvgTextRoot, IHasTransform
{
    public Matrix transform;


    public SvgText(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void setTransform(Matrix transform)
    {
        this.transform = transform;
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        SvgGraphicsElement.parseAttributesTransform(this, attributes);
    }
}
