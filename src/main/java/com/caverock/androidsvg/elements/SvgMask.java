package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgMask extends SvgConditionalContainer implements INotDirectlyRendered
{
    public Boolean maskUnitsAreUser;
    public Boolean maskContentUnitsAreUser;
    public SvgLength x;
    public SvgLength y;
    public SvgLength width;
    public SvgLength height;


    public SvgMask(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        SvgConditionalElement.parseAttributesConditional(this, attributes);
        parseAttributesMask(attributes);
    }


    private void parseAttributesMask(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case maskUnits:
                    if ("objectBoundingBox".equals(val))
                    {
                        maskUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        maskUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute maskUnits");
                    }
                    break;
                case maskContentUnits:
                    if ("objectBoundingBox".equals(val))
                    {
                        maskContentUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        maskContentUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute maskContentUnits");
                    }
                    break;
                case x:
                    x = SvgLength.parseLength(val);
                    break;
                case y:
                    y = SvgLength.parseLength(val);
                    break;
                case width:
                    width = SvgLength.parseLength(val);
                    if (width.isNegative())
                        throw new SAXException("Invalid <mask> element. width cannot be negative");
                    break;
                case height:
                    height = SvgLength.parseLength(val);
                    if (height.isNegative())
                        throw new SAXException("Invalid <mask> element. height cannot be negative");
                    break;
                default:
                    break;
            }
        }
    }
}
