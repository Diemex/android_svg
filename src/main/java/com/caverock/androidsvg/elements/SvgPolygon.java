package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;


public class SvgPolygon extends SvgPolyLine
{
    public SvgPolygon(SVG document, ISvgContainer parent)
    {
        super(document, parent);
        tag = "polygon"; //TODO verify this works
    }
}
