package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

public class SvgTSpan extends SvgTextPositionedContainer implements ISvgTextChild
{
    private ISvgTextRoot textRoot;


    public SvgTSpan(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void setTextRoot(ISvgTextRoot obj)
    {
        this.textRoot = obj;
    }


    @Override
    public ISvgTextRoot getTextRoot()
    {
        return this.textRoot;
    }
}
