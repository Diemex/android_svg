package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

// Any object that can be part of the tree
public class SvgObject
{
    public SVG document;
    public ISvgContainer parent;


    public SvgObject()
    {

    }


    public SvgObject(SVG document, ISvgContainer parent)
    {
        this.document = document;
        this.parent = parent;
    }


    public String toString()
    {
        return this.getClass().getSimpleName();
        //return super.toString();
    }
}
