package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgTextContainer extends SvgConditionalContainer
{
    public SvgTextContainer(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void addChild(SvgObject elem) throws SAXException
    {
        if (elem instanceof ISvgTextChild)
            children.add(elem);
        else
            throw new SAXException("Text content elements cannot contain " + elem + " elements.");
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
        SvgConditionalElement.parseAttributesConditional(this, attributes);
    }
}
