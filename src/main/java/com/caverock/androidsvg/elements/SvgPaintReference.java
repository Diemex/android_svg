package com.caverock.androidsvg.elements;

public class SvgPaintReference extends SvgPaint
{
    public String href;
    public SvgPaint fallback;


    public SvgPaintReference(String href, SvgPaint fallback)
    {
        this.href = href;
        this.fallback = fallback;
    }


    public String toString()
    {
        return href + " " + fallback;
    }
}
