package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

public class SvgView extends SvgViewBoxContainer implements INotDirectlyRendered
{
    public SvgView(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }
}
