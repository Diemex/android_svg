package com.caverock.androidsvg.elements;

// Special version of Colour that indicates use of 'currentColor' keyword
public class CurrentColor extends SvgColour
{
    private static CurrentColor instance = new CurrentColor();


    CurrentColor()
    {
    }


    public static CurrentColor getInstance()
    {
        return instance;
    }
}
