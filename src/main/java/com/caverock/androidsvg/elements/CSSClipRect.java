package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.TextScanner;

import org.xml.sax.SAXException;

import java.util.Locale;

public class CSSClipRect
{
    public SvgLength top;
    public SvgLength right;
    public SvgLength bottom;
    public SvgLength left;


    public CSSClipRect(SvgLength top, SvgLength right, SvgLength bottom, SvgLength left)
    {
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
    }


    // Parse CSS clip shape (always a rect())
    public static CSSClipRect parseClip(String val) throws SAXException
    {
        if ("auto".equals(val))
            return null;
        if (!val.toLowerCase(Locale.US).startsWith("rect("))
            throw new SAXException("Invalid clip attribute shape. Only rect() is supported.");

        TextScanner scan = new TextScanner(val.substring(5));
        scan.skipWhitespace();

        SvgLength top = parseLengthOrAuto(scan);
        scan.skipCommaWhitespace();
        SvgLength right = parseLengthOrAuto(scan);
        scan.skipCommaWhitespace();
        SvgLength bottom = parseLengthOrAuto(scan);
        scan.skipCommaWhitespace();
        SvgLength left = parseLengthOrAuto(scan);

        scan.skipWhitespace();
        if (!scan.consume(')'))
            throw new SAXException("Bad rect() clip definition: " + val);

        return new CSSClipRect(top, right, bottom, left);
    }


    private static SvgLength parseLengthOrAuto(TextScanner scan)
    {
        if (scan.consume("auto"))
            return new SvgLength(0f);

        return scan.nextLength();
    }
}
