package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgPattern extends SvgViewBoxContainer implements INotDirectlyRendered
{
    public Boolean patternUnitsAreUser;
    public Boolean patternContentUnitsAreUser;
    public Matrix patternTransform;
    public SvgLength x;
    public SvgLength y;
    public SvgLength width;
    public SvgLength height;
    public String href;


    public SvgPattern(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesPattern(attributes);
        parseAttributesStyle(this, attributes);
    }


    private void parseAttributesPattern(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case patternUnits:
                    if ("objectBoundingBox".equals(val))
                    {
                        patternUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        patternUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute patternUnits");
                    }
                    break;
                case patternContentUnits:
                    if ("objectBoundingBox".equals(val))
                    {
                        patternContentUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        patternContentUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute patternContentUnits");
                    }
                    break;
                case patternTransform:
                    patternTransform = SvgGraphicsElement.parseTransformList(val);
                    break;
                case x:
                    x = SvgLength.parseLength(val);
                    break;
                case y:
                    y = SvgLength.parseLength(val);
                    break;
                case width:
                    width = SvgLength.parseLength(val);
                    if (width.isNegative())
                        throw new SAXException("Invalid <pattern> element. width cannot be negative");
                    break;
                case height:
                    height = SvgLength.parseLength(val);
                    if (height.isNegative())
                        throw new SAXException("Invalid <pattern> element. height cannot be negative");
                    break;
                case href:
                    if (!SVGParser.XLINK_NAMESPACE.equals(attributes.getURI(i)))
                        break;
                    href = val;
                    break;
                default:
                    break;
            }
        }
    }
}
