package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgClipPath extends SvgGroup implements INotDirectlyRendered
{
    public Boolean clipPathUnitsAreUser;


    public SvgClipPath(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesClipPath(attributes);
    }


    private void parseAttributesClipPath(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case clipPathUnits:
                    if ("objectBoundingBox".equals(val))
                    {
                        clipPathUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        clipPathUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute clipPathUnits");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
