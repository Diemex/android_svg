package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.List;

public class SvgSolidColor extends SvgElementBase implements ISvgContainer
{
    public SvgLength solidColor;
    public SvgLength solidOpacity;


    public SvgSolidColor(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    // Dummy container methods. Stop is officially a container, but we
    // are not interested in any of its possible child elements.
    @Override
    public List<SvgObject> getChildren()
    {
        return SVG.EMPTY_CHILD_LIST;
    }


    @Override
    public void addChild(SvgObject elem) throws SAXException
    { /* do nothing */ }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesStyle(this, attributes);
    }
}
