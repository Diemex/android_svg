package com.caverock.androidsvg.elements;

import android.graphics.Matrix;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.TextScanner;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

// One of the element types that can cause graphics to be drawn onto the target canvas.
// Specifically: ‘circle’, ‘ellipse’, ‘line’, ‘path’, ‘polygon’, ‘polyline’ and ‘rect’
public abstract class SvgGraphicsElement extends SvgConditionalElement implements IHasTransform
{
    public Matrix transform;


    public SvgGraphicsElement(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    public static void parseAttributesTransform(IHasTransform obj, Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            if (SvgAttr.fromString(attributes.getLocalName(i)) == SvgAttr.transform)
            {
                obj.setTransform(parseTransformList(attributes.getValue(i)));
            }
        }
    }


    public static Matrix parseTransformList(String val) throws SAXException
    {
        Matrix matrix = new Matrix();

        TextScanner scan = new TextScanner(val);
        scan.skipWhitespace();

        while (!scan.empty())
        {
            String cmd = scan.nextFunction();

            if (cmd == null)
                throw new SAXException("Bad transform function encountered in transform list: " + val);

            if (cmd.equals("matrix"))
            {
                scan.skipWhitespace();
                Float a = scan.nextFloat();
                scan.skipCommaWhitespace();
                Float b = scan.nextFloat();
                scan.skipCommaWhitespace();
                Float c = scan.nextFloat();
                scan.skipCommaWhitespace();
                Float d = scan.nextFloat();
                scan.skipCommaWhitespace();
                Float e = scan.nextFloat();
                scan.skipCommaWhitespace();
                Float f = scan.nextFloat();
                scan.skipWhitespace();

                if (f == null || !scan.consume(')'))
                    throw new SAXException("Invalid transform list: " + val);

                Matrix m = new Matrix();
                m.setValues(new float[]{a, c, e, b, d, f, 0, 0, 1});
                matrix.preConcat(m);
            } else if (cmd.equals("translate"))
            {
                scan.skipWhitespace();
                Float tx = scan.nextFloat();
                Float ty = scan.possibleNextFloat();
                scan.skipWhitespace();

                if (tx == null || !scan.consume(')'))
                    throw new SAXException("Invalid transform list: " + val);

                if (ty == null)
                    matrix.preTranslate(tx, 0f);
                else
                    matrix.preTranslate(tx, ty);
            } else if (cmd.equals("scale"))
            {
                scan.skipWhitespace();
                Float sx = scan.nextFloat();
                Float sy = scan.possibleNextFloat();
                scan.skipWhitespace();

                if (sx == null || !scan.consume(')'))
                    throw new SAXException("Invalid transform list: " + val);

                if (sy == null)
                    matrix.preScale(sx, sx);
                else
                    matrix.preScale(sx, sy);
            } else if (cmd.equals("rotate"))
            {
                scan.skipWhitespace();
                Float ang = scan.nextFloat();
                Float cx = scan.possibleNextFloat();
                Float cy = scan.possibleNextFloat();
                scan.skipWhitespace();

                if (ang == null || !scan.consume(')'))
                    throw new SAXException("Invalid transform list: " + val);

                if (cx == null)
                {
                    matrix.preRotate(ang);
                } else if (cy != null)
                {
                    matrix.preRotate(ang, cx, cy);
                } else
                {
                    throw new SAXException("Invalid transform list: " + val);
                }
            } else if (cmd.equals("skewX"))
            {
                scan.skipWhitespace();
                Float ang = scan.nextFloat();
                scan.skipWhitespace();

                if (ang == null || !scan.consume(')'))
                    throw new SAXException("Invalid transform list: " + val);

                matrix.preSkew((float) Math.tan(Math.toRadians(ang)), 0f);
            } else if (cmd.equals("skewY"))
            {
                scan.skipWhitespace();
                Float ang = scan.nextFloat();
                scan.skipWhitespace();

                if (ang == null || !scan.consume(')'))
                    throw new SAXException("Invalid transform list: " + val);

                matrix.preSkew(0f, (float) Math.tan(Math.toRadians(ang)));
            } else if (cmd != null)
            {
                throw new SAXException("Invalid transform list fn: " + cmd + ")");
            }

            if (scan.empty())
                break;
            scan.skipCommaWhitespace();
        }

        return matrix;
    }


    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        //TODO Handle Exceptions correctly
        parseAttributesStyle(this, attributes);
        parseAttributesTransform(this, attributes);
        parseAttributesConditional(this, attributes);
    }


    @Override
    public void setTransform(Matrix transform)
    {
        this.transform = transform;
    }
}
