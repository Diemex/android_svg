package com.caverock.androidsvg.elements;

public class SvgTextSequence extends SvgObject implements ISvgTextChild
{
    public String text;

    private ISvgTextRoot textRoot;


    public SvgTextSequence(String text)
    {
        this.text = text;
    }


    public String toString()
    {
        return this.getClass().getSimpleName() + " '" + text + "'";
    }


    @Override
    public void setTextRoot(ISvgTextRoot obj)
    {
        this.textRoot = obj;
    }


    @Override
    public ISvgTextRoot getTextRoot()
    {
        return this.textRoot;
    }
}
