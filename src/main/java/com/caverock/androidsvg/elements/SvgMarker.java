package com.caverock.androidsvg.elements;

import com.caverock.androidsvg.NumberParser;
import com.caverock.androidsvg.SVG;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class SvgMarker extends SvgViewBoxContainer implements INotDirectlyRendered
{
    public boolean markerUnitsAreUser;
    public SvgLength refX;
    public SvgLength refY;
    public SvgLength markerWidth;
    public SvgLength markerHeight;
    public Float orient;


    public SvgMarker(SVG document, ISvgContainer parent)
    {
        super(document, parent);
    }


    @Override
    public void parseAttributes(Attributes attributes) throws SAXException
    {
        super.parseAttributes(attributes);
        parseAttributesMarker(attributes);
        parseAttributesStyle(this, attributes);
    }


    private void parseAttributesMarker(Attributes attributes) throws SAXException
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case refX:
                    refX = SvgLength.parseLength(val);
                    break;
                case refY:
                    refY = SvgLength.parseLength(val);
                    break;
                case markerWidth:
                    markerWidth = SvgLength.parseLength(val);
                    if (markerWidth.isNegative())
                        throw new SAXException("Invalid <marker> element. markerWidth cannot be negative");
                    break;
                case markerHeight:
                    markerHeight = SvgLength.parseLength(val);
                    if (markerHeight.isNegative())
                        throw new SAXException("Invalid <marker> element. markerHeight cannot be negative");
                    break;
                case markerUnits:
                    if ("strokeWidth".equals(val))
                    {
                        markerUnitsAreUser = false;
                    } else if ("userSpaceOnUse".equals(val))
                    {
                        markerUnitsAreUser = true;
                    } else
                    {
                        throw new SAXException("Invalid value for attribute markerUnits");
                    }
                    break;
                case orient:
                    if ("auto".equals(val))
                    {
                        orient = Float.NaN;
                    } else
                    {
                        orient = NumberParser.parseFloat(val);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
