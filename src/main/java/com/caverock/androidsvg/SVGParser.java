/*
   Copyright 2013 Paul LeBeau, Cave Rock Software Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.caverock.androidsvg;

import android.util.Log;

import com.caverock.androidsvg.CSSParser.MediaType;
import com.caverock.androidsvg.elements.ISvgContainer;
import com.caverock.androidsvg.elements.ISvgTextChild;
import com.caverock.androidsvg.elements.ISvgTextRoot;
import com.caverock.androidsvg.elements.SvgAttr;
import com.caverock.androidsvg.elements.SvgCircle;
import com.caverock.androidsvg.elements.SvgClipPath;
import com.caverock.androidsvg.elements.SvgConditionalContainer;
import com.caverock.androidsvg.elements.SvgDefs;
import com.caverock.androidsvg.elements.SvgElem;
import com.caverock.androidsvg.elements.SvgEllipse;
import com.caverock.androidsvg.elements.SvgGradientElement;
import com.caverock.androidsvg.elements.SvgGroup;
import com.caverock.androidsvg.elements.SvgImage;
import com.caverock.androidsvg.elements.SvgLine;
import com.caverock.androidsvg.elements.SvgLinearGradient;
import com.caverock.androidsvg.elements.SvgMarker;
import com.caverock.androidsvg.elements.SvgMask;
import com.caverock.androidsvg.elements.SvgObject;
import com.caverock.androidsvg.elements.SvgPath;
import com.caverock.androidsvg.elements.SvgPattern;
import com.caverock.androidsvg.elements.SvgPolyLine;
import com.caverock.androidsvg.elements.SvgPolygon;
import com.caverock.androidsvg.elements.SvgRadialGradient;
import com.caverock.androidsvg.elements.SvgRect;
import com.caverock.androidsvg.elements.SvgSolidColor;
import com.caverock.androidsvg.elements.SvgStop;
import com.caverock.androidsvg.elements.SvgSwitch;
import com.caverock.androidsvg.elements.SvgSymbol;
import com.caverock.androidsvg.elements.SvgTRef;
import com.caverock.androidsvg.elements.SvgTSpan;
import com.caverock.androidsvg.elements.SvgTag;
import com.caverock.androidsvg.elements.SvgText;
import com.caverock.androidsvg.elements.SvgTextContainer;
import com.caverock.androidsvg.elements.SvgTextPath;
import com.caverock.androidsvg.elements.SvgTextSequence;
import com.caverock.androidsvg.elements.SvgUse;
import com.caverock.androidsvg.elements.SvgView;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.DefaultHandler2;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * SVG parser code. Used by SVG class. Should not be called directly.
 *
 * @hide
 */
public class SVGParser extends DefaultHandler2
{
    public static final String TAG = "SVGParser";

    private static final String SVG_NAMESPACE = "http://www.w3.org/2000/svg";
    public static final String XLINK_NAMESPACE = "http://www.w3.org/1999/xlink";
    public static final String FEATURE_STRING_PREFIX = "http://www.w3.org/TR/SVG11/feature#";

    // SVG parser
    private SVG svgDocument = null;
    private ISvgContainer currentElement = null;

    // For handling elements we don't support
    private boolean ignoring = false;
    private int ignoreDepth;

    // For handling <title> and <desc>
    private boolean inMetadataElement = false;
    private SvgElem metadataTag = null;
    private StringBuilder metadataElementContents = null;

    // For handling <style>
    private boolean inStyleElement = false;
    private StringBuilder styleElementContents = null;

    private HashSet<String> supportedFormats = null;


    // Special attribute keywords
    public static final String NONE = "none";
    //TODO private->public
    public static final String CURRENTCOLOR = "currentColor";

    public static final String VALID_DISPLAY_VALUES = "|inline|block|list-item|run-in|compact|marker|table|inline-table" +
            "|table-row-group|table-header-group|table-footer-group|table-row" +
            "|table-column-group|table-column|table-cell|table-caption|none|";
    public static final String VALID_VISIBILITY_VALUES = "|visible|hidden|collapse|";


    protected void setSupportedFormats(String[] mimeTypes)
    {
        this.supportedFormats = new HashSet<String>(mimeTypes.length);
        Collections.addAll(this.supportedFormats, mimeTypes);
    }


    //=========================================================================
    // Main parser invocation methods
    //=========================================================================


    protected SVG parse(InputStream is) throws SVGParseException
    {
        // Transparently handle zipped files (.svgz)
        if (!is.markSupported())
        {
            // We need a a buffered stream so we can use mark() and reset()
            is = new BufferedInputStream(is);
        }
        try
        {
            is.mark(3);
            int firstTwoBytes = is.read() + (is.read() << 8);
            is.reset();
            if (firstTwoBytes == GZIPInputStream.GZIP_MAGIC)
            {
                // Looks like a zipped file.
                is = new GZIPInputStream(is);
            }
        } catch (IOException ioe)
        {
            // Not a zipped SVG. Fall through and try parsing it normally.
        }

        // Invoke the SAX XML parser on the input.
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try
        {
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            xr.setContentHandler(this);
            xr.setProperty("http://xml.org/sax/properties/lexical-handler", this);
            xr.parse(new InputSource(is));
        } catch (IOException e)
        {
            throw new SVGParseException("File error", e);
        } catch (ParserConfigurationException e)
        {
            throw new SVGParseException("XML Parser problem", e);
        } catch (SAXException e)
        {
            throw new SVGParseException("SVG parse error: " + e.getMessage(), e);
        } finally
        {
            try
            {
                is.close();
            } catch (IOException e)
            {
                Log.e(TAG, "Exception thrown closing input stream");
            }
        }
        return svgDocument;
    }


    //=========================================================================
    // SAX methods
    //=========================================================================


    @Override
    public void startDocument() throws SAXException
    {
        svgDocument = new SVG();
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        if (ignoring)
        {
            ignoreDepth++;
            return;
        }
        if (!SVG_NAMESPACE.equals(uri) && !"".equals(uri))
        {
            return;
        }

        SvgElem elem = SvgElem.fromString(localName);
        if (currentElement == null && elem != SvgElem.svg) //everything but <svg> needs a parent
            throw new SAXException("Invalid document. Root element must be <svg>");
        SvgObject obj;
        switch (elem)
        {
            //SvgViewBoxContainers
            case svg:
                debug("<svg>");
                obj = new SvgTag(svgDocument, currentElement);
                ((SvgTag) obj).parseAttributes(attributes);
                if (currentElement == null)
                    svgDocument.setRootElement(((SvgTag) obj));
                else currentElement.addChild(obj);
                currentElement = (SvgTag) obj;
                break;
            case symbol:
                debug("<symbol>");
                obj = new SvgSymbol(svgDocument, currentElement);
                ((SvgSymbol) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = ((SvgSymbol) obj);
                break;
            case marker:
                debug("<marker>");
                obj = new SvgMarker(svgDocument, currentElement);
                ((SvgMarker) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgMarker) obj;
                break;
            case pattern:
                debug("<pattern>");
                obj = new SvgPattern(svgDocument, currentElement);
                ((SvgPattern) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgPattern) obj;
                break;
            case view:
                debug("<view>");
                obj = new SvgView(svgDocument, currentElement);
                ((SvgView) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgView) obj;
                break;

            //SvgGroups
            case g:
            case a: // <a> treated like a group element
                debug("<g>");
                obj = new SvgGroup(svgDocument, currentElement);
                ((SvgGroup) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgGroup) obj;
                break;
            case defs:
                debug("<defs>");
                obj = new SvgDefs(svgDocument, currentElement);
                ((SvgDefs) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgDefs) obj;
                break;
            case use:
                debug("<use>");
                obj = new SvgUse(svgDocument, currentElement);
                ((SvgUse) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgUse) obj;
                break;
            case SWITCH:
                debug("<switch>");
                obj = new SvgSwitch(svgDocument, currentElement);
                ((SvgSwitch) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgSwitch) obj;
                break;
            case clipPath:
                debug("<clipPath>");
                obj = new SvgClipPath(svgDocument, currentElement);
                ((SvgClipPath) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgClipPath) obj;
                break;

            //SvgGraphicsElements
            case path:
                debug("<path>");
                obj = new SvgPath(svgDocument, currentElement);
                ((SvgPath) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;
            case rect:
                debug("<rect>");
                obj = new SvgRect(svgDocument, currentElement);
                ((SvgRect) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;
            case circle:
                debug("<circle>");
                obj = new SvgCircle(svgDocument, currentElement);
                ((SvgCircle) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;
            case ellipse:
                debug("<ellipse>");
                obj = new SvgEllipse(svgDocument, currentElement);
                ((SvgEllipse) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;
            case line:
                debug("<line>");
                obj = new SvgLine(svgDocument, currentElement);
                ((SvgLine) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;
            case polyline:
                debug("<polyline>");
                obj = new SvgPolyLine(svgDocument, currentElement);
                ((SvgPolyLine) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;
            case polygon:
                debug("<polygon>");
                obj = new SvgPolygon(svgDocument, currentElement);
                ((SvgPolygon) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                break;

            //SvgTextContainers
            case text:
                debug("<text>");
                obj = new SvgText(svgDocument, currentElement);
                ((SvgText) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgText) obj;
                break;
            case tspan:
                debug("<tspan>");
                if (!(currentElement instanceof SvgTextContainer))
                    throw new SAXException("Invalid document. <tspan> elements are only valid inside <text> or other <tspan> elements.");
                obj = new SvgTSpan(svgDocument, currentElement);
                ((SvgTSpan) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgTSpan) obj;
                if (obj.parent instanceof ISvgTextRoot)
                    ((SvgTSpan) obj).setTextRoot((ISvgTextRoot) obj.parent);
                else
                    ((SvgTSpan) obj).setTextRoot(((ISvgTextChild) obj.parent).getTextRoot());
                break;
            case tref:
                debug("<tref>");
                if (!(currentElement instanceof SvgTextContainer))
                    throw new SAXException("Invalid document. <tref> elements are only valid inside <text> or <tspan> elements.");
                obj = new SvgTRef(svgDocument, currentElement);
                ((SvgTRef) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                if (((SvgTRef) obj).parent instanceof ISvgTextRoot)
                    ((SvgTRef) obj).setTextRoot((ISvgTextRoot) obj.parent);
                else
                    ((SvgTRef) obj).setTextRoot(((ISvgTextChild) obj.parent).getTextRoot());
                break;
            case textPath:
                debug("<textPath>");
                obj = new SvgTextPath(svgDocument, currentElement);
                ((SvgTextPath) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgTextPath) obj;
                if (obj.parent instanceof ISvgTextRoot)
                    ((SvgTextPath) obj).setTextRoot((ISvgTextRoot) obj.parent);
                else
                    ((SvgTextPath) obj).setTextRoot(((ISvgTextChild) obj.parent).getTextRoot());
                break;

            //SvgGradientElements
            case linearGradient:
                debug("<linearGradiant>");
                obj = new SvgLinearGradient(svgDocument, currentElement);
                ((SvgLinearGradient) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgLinearGradient) obj;
                break;
            case radialGradient:
                debug("<radialGradient>");
                obj = new SvgRadialGradient(svgDocument, currentElement);
                ((SvgRadialGradient) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgRadialGradient) obj;
                break;

            //SvgElementBase Classes
            case stop:
                debug("<stop>");
                if (!(currentElement instanceof SvgGradientElement))
                    throw new SAXException("Invalid document. <stop> elements are only valid inside <linearGradiant> or <radialGradient> elements.");
                obj = new SvgStop(svgDocument, currentElement);
                ((SvgStop) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgStop) obj;
                break;
            case solidColor:
                debug("<solidColor>");
                obj = new SvgSolidColor(svgDocument, currentElement);
                ((SvgSolidColor) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgSolidColor) obj;
                break;

            //Metadata and special handling
            case title:
            case desc:
                inMetadataElement = true;
                metadataTag = elem;
                break;
            case style: //css
                style(attributes);
                break;

            case image:
                debug("<image>");
                obj = new SvgImage(svgDocument, currentElement);
                ((SvgImage) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgImage) obj;
                break;
            case mask:
                debug("<mask>");
                obj = new SvgMask(svgDocument, currentElement);
                ((SvgMask) obj).parseAttributes(attributes);
                currentElement.addChild(obj);
                currentElement = (SvgMask) obj;
                break;
            default:
                ignoring = true;
                ignoreDepth = 1;
                break;
        }
    }


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        if (ignoring)
            return;

        if (inMetadataElement)
        {
            if (metadataElementContents == null)
                metadataElementContents = new StringBuilder(length);
            metadataElementContents.append(ch, start, length);
            return;
        }

        if (inStyleElement)
        {
            if (styleElementContents == null)
                styleElementContents = new StringBuilder(length);
            styleElementContents.append(ch, start, length);
            return;
        }

        if (currentElement instanceof SvgTextContainer)
        {
            // The SAX parser can pass us several text nodes in a row. If this happens, we
            // want to collapse them all into one SVG.TextSequence node
            SvgConditionalContainer parent = (SvgConditionalContainer) currentElement;
            int numOlderSiblings = parent.children.size();
            SvgObject previousSibling = (numOlderSiblings == 0) ? null : parent.children.get(numOlderSiblings - 1);
            if (previousSibling instanceof SvgTextSequence)
            {
                // Last sibling was a TextSequence also, so merge them.
                ((SvgTextSequence) previousSibling).text += new String(ch, start, length);
            } else
            {
                // Add a new TextSequence to the child node list
                ((SvgConditionalContainer) currentElement).addChild(new SvgTextSequence(new String(ch, start, length)));
            }
        }

    }


    @Override
    public void comment(char[] ch, int start, int length) throws SAXException
    {
        if (ignoring)
            return;

        // It is legal for the contents of the <style> element to be enclosed in XML comments (ie. "<!--" and "-->").
        // So we need to include the contents of the comment in the text sent to the CSS parser.
        if (inStyleElement)
        {
            if (styleElementContents == null)
                styleElementContents = new StringBuilder(length);
            styleElementContents.append(ch, start, length);
            return;
        }

    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if (ignoring)
        {
            if (--ignoreDepth == 0)
            {
                ignoring = false;
                return;
            }
        }

        if (!SVG_NAMESPACE.equals(uri) && !"".equals(uri))
        {
            return;
        }

        switch (SvgElem.fromString(localName))
        {
            case title:
            case desc:
                inMetadataElement = false;
                if (metadataTag == SvgElem.title)
                    svgDocument.setTitle(metadataElementContents.toString());
                else if (metadataTag == SvgElem.desc)
                    svgDocument.setDesc(metadataElementContents.toString());
                metadataElementContents.setLength(0);
                return;

            case style:
                if (styleElementContents != null)
                {
                    inStyleElement = false;
                    parseCSSStyleSheet(styleElementContents.toString());
                    styleElementContents.setLength(0);
                    return;
                }
                break;

            case svg:
            case defs:
            case g:
            case use:
            case image:
            case text:
            case tspan:
            case SWITCH:
            case symbol:
            case marker:
            case linearGradient:
            case radialGradient:
            case stop:
            case clipPath:
            case textPath:
            case pattern:
            case view:
            case mask:
            case solidColor:
                currentElement = ((SvgObject) currentElement).parent;
                break;

            default:
                // no action
        }

    }


    @Override
    public void endDocument() throws SAXException
    {
        // Dump document
        if (LibConfig.DEBUG)
            dumpNode(svgDocument.getRootElement(), "");
    }


    private void dumpNode(SvgObject elem, String indent)
    {
        Log.d(TAG, indent + elem);
        if (elem instanceof SvgConditionalContainer)
        {
            indent = indent + "  ";
            for (SvgObject child : ((SvgConditionalContainer) elem).children)
            {
                dumpNode(child, indent);
            }
        }
    }


    private void debug(String format, Object... args)
    {
        if (LibConfig.DEBUG)
            Log.d(TAG, String.format(format, args));
    }


    //=========================================================================
    // Parsing <style> element. Very basic CSS parser.
    //=========================================================================


    private void style(Attributes attributes) throws SAXException
    {
        debug("<style>");

        // Check style sheet is in CSS format
        boolean isTextCSS = true;
        String media = "all";

        for (int i = 0; i < attributes.getLength(); i++)
        {
            String val = attributes.getValue(i).trim();
            switch (SvgAttr.fromString(attributes.getLocalName(i)))
            {
                case type:
                    isTextCSS = val.equals("text/css");
                    break;
                case media:
                    media = val;
                    break;
                default:
                    break;
            }
        }

        if (isTextCSS && CSSParser.mediaMatches(media, MediaType.screen))
        {
            inStyleElement = true;
        } else
        {
            ignoring = true;
            ignoreDepth = 1;
        }
    }


    private void parseCSSStyleSheet(String sheet) throws SAXException
    {
        CSSParser cssp = new CSSParser(MediaType.screen);
        svgDocument.addCSSRules(cssp.parse(sheet));
    }

}
